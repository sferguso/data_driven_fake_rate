#ifndef efitplot
#define efitplot

std::vector<TCanvas*> EFitPlotRatio(TH1 *&data, TH1 *&data2, TH1 *&mcp1, TH1 *&mcp2, TH1 *&mcp3, TH1 *&fit, TVectorD results,
			       int IsData, TString highpT, TString etaRange="0_eta_12", TString RJet="0.4");


#endif
