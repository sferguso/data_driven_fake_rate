#***************** To run EFitAnalysis.C *********************
##loop over:
## pT slices
## dR
## template source

FitRanges="FitRange5" # FitRange2 FitRange3 FitRange4" #FitRange5 FitRange6 FitRange7 FitRange8 FitRange9 FitRange10" # FitRange2" # FitRange3 FitRange4 FitRange5 FitRange6 FitRange7 FitRange8 FitRange9" #for data

dRs="0 1 2"

refpTs="200_400_GeV" #pT 200_400_GeV 10_20_200_400_GeV" #default

#pTs="200_400_GeV 400_600_GeV 600_800_GeV 800_1000_GeV 1000_1200_GeV 1200_1400_GeV 1400_1600_GeV 1600_1800_GeV 1800_2000_GeV 2000_2200_GeV 2200_2400_GeV 2400_2600_GeV 2600_2800_GeV 2800_3000_GeV"
#pTs="0_10_200_400_GeV 10_20_200_400_GeV 20_30_200_400_GeV 30_40_200_400_GeV"
pTs="200_400_GeV 400_600_GeV 600_800_GeV 800_1000_GeV 1000_1200_GeV 1200_1400_GeV 1400_1600_GeV 1600_1800_GeV 1800_2000_GeV 2000_2200_GeV 2200_2400_GeV 2400_2600_GeV 2600_2800_GeV 2800_3000_GeV"

layers="Blayer" #IBL layer1 layer2"
calib="_calib"
region="in"

#template_sample="/scratch/ATLAS/Tracking/samples/MC15/slice_pythia_all.root"
template_sample="default"

samples="/scratch/ATLAS/Tracking/tracking_samples/rel21/MC16/slice_pythia_MC16.root /scratch/ATLAS/Tracking/tracking_samples/rel21/MC16/slice_pythia_MC16c.root /scratch/ATLAS/Tracking/tracking_samples/rel21/data_16/slice_data_2016_stable.root /scratch/ATLAS/Tracking/tracking_samples/rel21/data_16/slice_data_2016_all.root /scratch/ATLAS/Tracking/tracking_samples/rel21/data_17/data_2017_all.root" 

samples="/scratch/ATLAS/Tracking/tracking_samples/rel21/MC16/slice_pythia_MC16c.root /scratch/ATLAS/Tracking/tracking_samples/rel21/data_16/slice_data_2016_stable.root /scratch/ATLAS/Tracking/tracking_samples/rel21/data_17/data_2017_all.root" 


#`/bin/ls /scratch/ATLAS/Tracking/tracking_samples/rel21/MC16_pTscan//slice_pythia_*.root`"
#samples="/scratch/ATLAS/Tracking/tracking_samples/rel21//MC16c_pT5/slice_pythia_all.root"  # `/bin/ls /scratch/ATLAS/Tracking/tracking_samples/rel21/data_1*_pT5/slice_data_201*.root` /scratch/ATLAS/Tracking/tracking_samples/rel21/data_17_pT5/data_2017_all.root  /scratch/ATLAS/Tracking/tracking_samples/rel21/data_16_pT5/data_2016_all.root"


#MC16_2017_conditions/pythia_r9600_JZ67.root /scratch/ATLAS/Tracking/tracking_samples/rel21/MC16_2017_conditions/pythia_r9601_JZ67.root /scratch/ATLAS/Tracking/tracking_samples/rel21/MC16/pythia_JZ67.root"


#scratch/ATLAS/Tracking/tracking_samples/rel21//MC16/slice_pythia_all.root /scratch/ATLAS/Tracking/tracking_samples/rel21//MC16_lowpT/slice_pythia_all_lowpT.root"

#data17/slice_data_2017_all.root" #/scratch/ATLAS/Tracking/tracking_samples/rel21/data16/slice_data_2016_all.root" # /scratch/ATLAS/Tracking/tracking_samples/rel21/MC16/slice_pythia_all.root"

# /scratch/ATLAS/Tracking/samples/slice_herwigpp_all.root /scratch/ATLAS/Tracking/samples/slice_sherpa_all.root

dR="0";

for layer in $layers ; do
    for FitRange in $FitRanges ; do
    for sample in $samples ; do
	for refpT in $refpTs ; do
	    for pT in $pTs ; do
		for dR in $dRs ; do
		    
		    echo $sample
		    
		    samp=`basename $sample`
		    samp=${samp%.root}

		    temp_samp=""
		    [[ $template_sample != "default" ]] && temp_samp=`basename $template_sample` && temp_samp=_${temp_samp%.root}

		  #  [[ $sample != *"data"* ]] && calib=""

		    outname="${samp}${calib}${temp_samp}_${layer}_${FitRange}_${region}_dR${dR}_${pT}_ref_${refpT}"
		    [ "$dR" == "0" ] && dR="" &&  outname="${samp}${calib}${temp_samp}_${layer}_${FitRange}_${region}_${pT}_ref_${refpT}"
		    rootname="/scratch/ATLAS/Tracking/output/$outname.root"
		    
		    #	echo $outname $rootname
		    
		    #now deal with various special cases to reproduce the paper fit:
		    #first, the pT slice to take the templates from:
		    reftmp=$refpT
		    [ $refpT == "mudep" ] && [[ $pT == "0_10_"* ]] && reftmp="0_10_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "10_20_"* ]] && reftmp="10_20_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "20_30_"* ]] && reftmp="20_30_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "30_40_"* ]] && reftmp="30_40_200_400_GeV"

		    #for MC in the old method, always use the same pT slice as is being fitted:
		    [[ $refpT == "default" ]] && [[ $sample != *"data"* ]] && reftmp=$pT    
		    [ $refpT == "pT" ] && reftmp=$pT
		    
		    #now the fit ranges:

		    [[ $FitRange = "FitRange1" ]] && FitLow=1.6 && FitHigh=4
		    [[ $FitRange = "FitRange2" ]] && FitLow=1.4 && FitHigh=4
		    [[ $FitRange = "FitRange3" ]] && FitLow=1.3 && FitHigh=4
		    [[ $FitRange = "FitRange4" ]] && FitLow=1.2 && FitHigh=4
		    [[ $FitRange = "FitRange5" ]] && FitLow=1.1 && FitHigh=4
		    
		   # [[ $FitRange = "FitRange2" ]] && FitLow=1.6 && FitHigh=3.
		   # [[ $FitRange = "FitRange3" ]] && FitLow=1.6 && FitHigh=5.
		   # [[ $FitRange = "FitRange4" ]] && FitLow=1.6 && FitHigh=6.
		   # [[ $FitRange = "FitRange5" ]] && FitLow=1.65 && FitHigh=4.
		   # [[ $FitRange = "FitRange6" ]] && FitLow=1.7 && FitHigh=4.
		   # [[ $FitRange = "FitRange7" ]] && FitLow=1.5 && FitHigh=4.
		   # [[ $FitRange = "FitRange8" ]] && FitLow=1.4 && FitHigh=4.
		   # [[ $FitRange = "FitRange9" ]] && FitLow=1.1 && FitHigh=3.2
		   #  [[ $FitRange = "FitRange10" ]] && FitLow=1.1 && FitHigh=2.7

		    # [[ $FitRange = "FitRange1" ]] && [[ $sample != *"data"* ]] && FitLow=1.1 && FitHigh=3.2
		    #[[ $FitRange = "FitRange2" ]] && [[ $sample != *"data"* ]] && FitLow=1.1 && FitHigh=3.0
		    #[[ $FitRange = "FitRange3" ]] && [[ $sample != *"data"* ]] && FitLow=1.1 && FitHigh=3.7
		    #[[ $FitRange = "FitRange4" ]] && [[ $sample != *"data"* ]] && FitLow=1.1 && FitHigh=4.2
		    #[[ $FitRange = "FitRange5" ]] && [[ $sample != *"data"* ]] && FitLow=1.2 && FitHigh=3.2
		    #[[ $FitRange = "FitRange6" ]] && [[ $sample != *"data"* ]] && FitLow=1.3 && FitHigh=3.2
		    #[[ $FitRange = "FitRange7" ]] && [[ $sample != *"data"* ]] && FitLow=1.0 && FitHigh=3.2
		    #[[ $FitRange = "FitRange8" ]] && [[ $sample != *"data"* ]] && FitLow=0.9 && FitHigh=3.2
		    #[[ $FitRange = "FitRange9" ]] && [[ $sample != *"data"* ]] && FitLow=1.6 && FitHigh=4.

		    #the files to load data and templates from:
		    data=$sample
		    template=$template_sample
		    [[ $template = "default" ]] && template=$sample
		    
		    #the new per-pixel fits
		    #data1H=$layer/$pT"/dEdx_in${dR}_ntrk1"
		    #data2H=$layer/$pT"/dEdx_in${dR}_ntrk2"
		    data1H=$layer/$pT"/dEdx_${region}${dR}_ntrk1${calib}"
		    data2H=$layer/$pT"/dEdx_${region}${dR}_ntrk2${calib}"
		    template1H=$layer/$reftmp"/dEdx_out${dR}_ntrk1${calib}"
		    template2H=$layer/$reftmp"/dEdx_in${dR}_ntrk2${calib}"		
		    template3H=$layer/$reftmp"/dEdx_in${dR}_ntrk3${calib}"		
		    
		    echo EFitAnalysis $data $data1H $data2H
		   echo $template $template1H $template2H $template3H
		   echo $rootname $FitLow $FitHigh $pT

		    EFitAnalysis $data $data1H $data2H \
				 $template $template1H $template2H $template3H  \
				 $rootname $FitLow $FitHigh $pT
		    
		done
	    done
	done
    done
done
done










