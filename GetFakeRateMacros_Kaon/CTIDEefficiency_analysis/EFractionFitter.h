

#ifndef __EFractionFitter__
#define __EFractionFitter__

#ifndef ROOT_TVirtualFitter
#include "TVirtualFitter.h"
#endif

#ifndef ROOT_TObjArray
#include "TObjArray.h"
#endif

#ifndef ROOT_TFitResultPtr
#include "TFitResultPtr.h"
#endif


#include "TH1.h"
#include <iostream>
#include <TROOT.h>
#include <vector>
#include "TMinuit.h"
#include "TGraph.h"
class TH1;

namespace ROOT {
	namespace Fit {
		class Fitter;
	}
}
class EFractionFitter  {
	
public:
	EFractionFitter();									//default constructor
	EFractionFitter(TH1* data, TObjArray* MCs);			//constructor
	virtual ~EFractionFitter();									//destructor
	void SetRangeX(Int_t low, Int_t high);
	void SetIntegralRangeX(Int_t low, Int_t high);
	void Constrain(Int_t parm, double low, double high);
	TFitResultPtr Fit();
	TGraph* Scan(unsigned int ivar, double xmin, double xmax);
	Double_t GetChisquare() const;
	Int_t GetNDF() const;
	TH1* GetPlot();
	TH1* GetPlotWithError();
	TH1* GetMCPrediction(Int_t parm) const;  //why is this one a const?
	void GetResult(Int_t parm, Double_t& value, Double_t& error) const;  //why is this one a const as well? 
	//Evaluate FCN
	Double_t EvaluateFCN(const Double_t* par);

private: 
	void CheckConsistency();
	void GetXRange(Int_t minX, Int_t maxX) const;
	void GetIntegralXRange(Int_t minX, Int_t maxX) const;
	void ComputeFCN(Double_t &f, const Double_t* par, Int_t flag);
	// static void MinuitFCN(Int_t& nDim, Double_t* gin, Double_t& result, Double_t *par, Int_t flag);
protected:
	Bool_t fFitDone;			//flags whether a fit has been completed
	TH1* fData;				//poInt_ter to the data histogram to be fit
	TObjArray fMCs;		//array of poInt_ters to the MC templates
	TObjArray fPs;			//array of poInt_ters to the predictions of real template distributions
	TH1* fPlot; 			//poInt_ter to histogram containing summed template predictions
	TH1* fPlotWithError; 			//poInt_ter to histogram containing summed template predictions
	Int_t fNtemp; 			//number of templates
	Int_t fLowLimitX;			//fit bin in X dimension
	Int_t fHighLimitX;		//last bin in X dimension
	Int_t fLowIntX;			//first Int_tegral bin in X dimension
	Int_t fHighIntX;			//last ingetral bin in X dimension
	Double_t fIntegralData;	//data histogram Int_tegral content
	Double_t* fIntegralMCs;   //mc histogram Int_tegral content
	Double_t* fFraction;		//template 1 fraction 
	ROOT::Fit::Fitter *eFractionFitter;	//poInt_ter to Fitter class
	Int_t fNpar;				//number of fit parameters (1) 
	Int_t fNpfits;				//number of points used in the fit
	// Int_t fNDF;					//number of degrees of freedom in the fit

	//ClassDef(EFractionFitter,1);
};



#endif //__EFractionFitter__



