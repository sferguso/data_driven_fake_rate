
 

##2017

#if [ -f user.hesketh.data17.00* ] ; then

#ibl jump up: 327490, L=35.432
hadd  -j -n 10000 slice_data_2017_1.root  \
     user.hesketh.data17.00325*/*.root \
     user.hesketh.data17.00326*/*.root \
     user.hesketh.data17.003270*/*.root \
     user.hesketh.data17.003271*/*.root \
     user.hesketh.data17.003272*/*.root \
     user.hesketh.data17.003273*/*.root



#another ibl jump: 332340, L =45
hadd  slice_data_2017_2.root  \
     user.hesketh.data17.003274*/*.root \
     user.hesketh.data17.003275*/*.root \
     user.hesketh.data17.003276*/*.root \
     user.hesketh.data17.003277*/*.root \
     user.hesketh.data17.003278*/*.root \
     user.hesketh.data17.00328*/*.root \
     user.hesketh.data17.00329*/*.root \
     user.hesketh.data17.00330*/*.root \
     user.hesketh.data17.00331*/*.root \
     user.hesketh.data17.00332303*/*.root

  #   user.hesketh.data17.003279*/*.root \


#beginning of the drop, runs less than 336506, L=53.7
hadd  -j -n 10000 slice_data_2017_3.root  \
     user.hesketh.data17.00332304*/*.root \
     user.hesketh.data17.003327*/*.root \
     user.hesketh.data17.003328*/*.root \
     user.hesketh.data17.003329*/*.root \
     user.hesketh.data17.00333*/*.root \
     user.hesketh.data17.00334*/*.root \
     user.hesketh.data17.003350*/*.root \
     user.hesketh.data17.003351*/*.root \
     user.hesketh.data17.003352*/*.root

  #   user.hesketh.data17.003324*/*.root \
  #   user.hesketh.data17.003325*/*.root \
  #   user.hesketh.data17.003326*/*.root \


#plateau at the end: runs over 336678, L =56
hadd  -j -n 10000 slice_data_2017_4.root  \
     user.hesketh.data17.003368*/*.root \
     user.hesketh.data17.003369*/*.root \
     user.hesketh.data17.00337*/*.root \


hadd  -j -n 10000 slice_data_2017_5.root  \
     user.hesketh.data17.00338*/*.root \
     user.hesketh.data17.00339*/*.root \
     user.hesketh.data17.0034*/*.root \





for dir in `/bin/ls -d user.hesketh.data*` ; do
    dircopy=$dir
    run=`echo $dir | cut -d "." -f 4`
    hadd -j -n 10000 data.$run.root *$run*/*
done

hadd -j -n 10000 data_2017_all.root data.00*.root
