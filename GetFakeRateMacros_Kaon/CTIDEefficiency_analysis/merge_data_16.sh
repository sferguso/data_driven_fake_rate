
#
#exclude run 297730, low mean dEdx
#first cut at <300540: mean dEdx step
#then <301912: ToT change
# then every 10-20 runs?


#if [ -f user.hesketh.data16.00* ] ; then

#exclude low mean dEdx run (297730), cover up to ToT change
hadd -j -n 10000 slice_data_2016_1.root  \
     user.hesketh.data16.00298*/*.root \
     user.hesketh.data16.00299*/*.root \
     user.hesketh.data16.003002*/*.root \
     user.hesketh.data16.003003*/*.root \
     user.hesketh.data16.003004*/*.root 


#Up to analog threshold change. Int lumi up to 2.53
hadd  -j -n 10000 slice_data_2016_2.root  \
     user.hesketh.data16.003005*/*.root \
     user.hesketh.data16.003006*/*.root \
     user.hesketh.data16.003007*/*.root \
     user.hesketh.data16.003008*/*.root \
     user.hesketh.data16.003009*/*.root 

#up to 15 fb-1: runs below 305*
hadd  -j -n 10000 slice_data_2016_3.root  \
     user.hesketh.data16.00301*/*.root \
     user.hesketh.data16.00302*/*.root \
     user.hesketh.data16.00303*/*.root \
     user.hesketh.data16.00304*/*.root \
     user.hesketh.data16.00305*/*.root 


#up to IBL HV change: lumi: 24.6, runs below 309*
hadd  -j -n 10000 slice_data_2016_4.root  \
     user.hesketh.data16.00306*/*.root \
     user.hesketh.data16.00307*/*.root \
     user.hesketh.data16.00308*/*.root

#last run in 2016: 311481, lumi 32
hadd  -j -n 10000 slice_data_2016_5.root  \
     user.hesketh.data16.00309*/*.root \
     user.hesketh.data16.0031*/*.root 



hadd -j -n 1000 slice_data_2016_stable.root slice_data_2016_3.root slice_data_2016_4.root slice_data_2016_5.root

                                                                                            
hadd -j -n 1000 slice_data_2016_all.root slice_data_2016_stable.root slice_data_2016_1.root slice_data_2016_2.root 
 

for dir in `/bin/ls -d user.hesketh.data*` ; do
    dircopy=$dir
    run=`echo $dir | cut -d "." -f 4`
    hadd -j -n 10000 data.$run.root *$run*/*
done

