#***************** To run EFitAnalysis.C *********************
##loop over:
## pT slices
## dR
## template source

FitRanges="FitRange1" # FitRange2 FitRange3 FitRange4 FitRange5 FitRange6 FitRange7 FitRange8 FitRange9 FitRange10" # FitRange2" # FitRange3 FitRange4 FitRange5 FitRange6 FitRange7 FitRange8 FitRange9" #for data

FitLows="1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.7 1.9 2.0"
FitHighs="3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0"

dRs="0" # 1 2"

refpTs="200_400_GeV" #pT 200_400_GeV 10_20_200_400_GeV" #default

pTs="200_400_GeV 400_600_GeV 600_800_GeV 800_1000_GeV 1000_1200_GeV"

layers="Blayer" #IBL Blayer layer1 layer2"
calib="_calib"
region="in"

#template_sample="/scratch/Work/Tracking/samples/MC15/slice_pythia_all.root"
template_sample="default"

#samples="/scratch/Work/Tracking/tracking_samples/rel21/data_17/data_2017_all.root"
samples="/scratch/Work/Tracking/tracking_samples/rel21/data_16/slice_data_2016_stable.root /scratch/Work/Tracking/tracking_samples/rel21/MC16/slice_pythia_MC16c.root" 


for layer in $layers ; do
    for FitRange in $FitRanges ; do
    for sample in $samples ; do
	for refpT in $refpTs ; do
	    for pT in $pTs ; do
		for dR in $dRs ; do
		    for FitLow in $FitLows ; do
			for FitHigh in $FitHighs ; do
		    
		    
		    echo $sample
		    
		    samp=`basename $sample`
		    samp=${samp%.root}
		    
		    temp_samp=""
		    [[ $template_sample != "default" ]] && temp_samp=`basename $template_sample` && temp_samp=_${temp_samp%.root}

		    [[ $sample != *"data"* ]] && calib=""

		    outname="${samp}${calib}${temp_samp}_${layer}_${FitLow}_${FitHigh}_${region}_dR${dR}_${pT}_ref_${refpT}"

		    tmpdR=$dR
		    [ "$dR" == "0" ] && tmpdR="" &&  outname="${samp}${calib}${temp_samp}_${layer}_${FitLow}_${FitHigh}_${region}_${pT}_ref_${refpT}"
		    rootname="/scratch/Work/Tracking/output/$outname.root"
		    
		    #	echo $outname $rootname
		    
		    #now deal with various special cases to reproduce the paper fit:
		    #first, the pT slice to take the templates from:
		    reftmp=$refpT
		    [ $refpT == "mudep" ] && [[ $pT == "0_10_"* ]] && reftmp="0_10_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "10_20_"* ]] && reftmp="10_20_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "20_30_"* ]] && reftmp="20_30_200_400_GeV"
		    [ $refpT == "mudep" ] && [[ $pT == "30_40_"* ]] && reftmp="30_40_200_400_GeV"

		    #for MC in the old method, always use the same pT slice as is being fitted:
		    [[ $refpT == "default" ]] && [[ $sample != *"data"* ]] && reftmp=$pT    
		    [ $refpT == "pT" ] && reftmp=$pT
		    
		    #the files to load data and templates from:
		    data=$sample
		    template=$template_sample
		    [[ $template = "default" ]] && template=$sample
		    
		    #the new per-pixel fits
		    #data1H=$layer/$pT"/dEdx_in${dR}_ntrk1"
		    #data2H=$layer/$pT"/dEdx_in${dR}_ntrk2"
		    data1H=$layer/$pT"/dEdx_${region}${tmpdR}_ntrk1${calib}"
		    data2H=$layer/$pT"/dEdx_${region}${tmpdR}_ntrk2${calib}"
		    template1H=$layer/$reftmp"/dEdx_out${tmpdR}_ntrk1${calib}"
		    template2H=$layer/$reftmp"/dEdx_in${tmpdR}_ntrk2${calib}"		
		    template3H=$layer/$reftmp"/dEdx_in${tmpdR}_ntrk3${calib}"		
		    
		    echo EFitAnalysis $data $data1H $data2H
		    echo $template $template1H $template2H $template3H
		    echo $rootname $FitLow $FitHigh $pT
		    
		    EFitAnalysis $data $data1H $data2H \
				 $template $template1H $template2H $template3H  \
				 $rootname $FitLow $FitHigh $pT

			done
		    done
		done
	    done
	done
    done
done
done










