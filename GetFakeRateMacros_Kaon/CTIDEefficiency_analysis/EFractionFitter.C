/**************************************
*
* Fitter class that fits two template
* histograms to a data histogram using
* a standard poisson likelihood fit 
* (no template statistics)
*
* E. Duffield (eduffield@berkeley.edu)
* Fri Apr 8 12:44:05 UTC+1 2016
*
***************************************/

#include "EFractionFitter.h"
#include "Math/Functor.h"
#include "Math/Minimizer.h"
#include "TH1.h"
#include "TMath.h"
#include "Fit/Fitter.h"
#include "Fit/FitConfig.h"
#include "TClass.h"
#include <fstream>
#include <iostream>
#include "TFitResult.h"
#include "TGraph.h"
#include "TObject.h"
#include "TString.h"

//ClassImp(EFractionFitter)

EFractionFitter::EFractionFitter():
fLowLimitX(0),fHighLimitX(0), fData(0), fPlot(0){
	fNpfits=0;
	fNpar=0;
}
EFractionFitter::EFractionFitter(TH1* data, TObjArray* MCs)
{
  fData = data;
  fNtemp = MCs->GetEntries();
  fNpar = fNtemp;
  fLowLimitX=0;	
  fHighLimitX=0;
  fLowIntX=0;
  fHighIntX=0;
	
  Int_t temp;
  for(temp=0; temp<fNtemp; temp++){
    fMCs.Add(MCs->At(temp)); 
    TString s = Form("Prediction for MC sample %i",temp);
    TH1* pred = (TH1*) ((TH1*)MCs->At(temp))->Clone(s);
    pred->SetTitle(s);
    fPs.Add(pred);
  }
  
  fIntegralMCs = new Double_t[fNtemp];
  fFraction = new Double_t;
  SetIntegralRangeX(-1,-1);

  eFractionFitter = new ROOT::Fit::Fitter();

  Double_t defaultFraction = data->Integral(data->FindBin(1.2), data->FindBin(3.2))/((Double_t)fNtemp); 
  Double_t defaultStep = 0.1;
  //setup parameters
  eFractionFitter->Config().MinimizerOptions().SetPrintLevel(2);
  
  std::vector<ROOT::Fit::ParameterSettings> & parameters = eFractionFitter->Config().ParamsSettings();
  parameters.reserve(fNpar);
  for (Int_t par = 0; par < fNpar; ++par) {
    TString name("frac"); name += par;
    parameters.push_back(ROOT::Fit::ParameterSettings(name.Data(), defaultFraction, defaultStep) );
  }
  // if (eFractionFitter->Config().MinimizerOptions().ErrorDef() == 1.0 ) //set for chi squared fit
  eFractionFitter->Config().MinimizerOptions().SetErrorDef(0.5);
  
  eFractionFitter->Config().SetParabErrors(true);
  
}

EFractionFitter::~EFractionFitter()
{
	if (eFractionFitter) delete eFractionFitter;
	delete[] fIntegralMCs;
	delete fFraction;
	if (fPlot) delete fPlot; 
	fPs.Delete();
}

void EFractionFitter::SetRangeX(Int_t low, Int_t high)
{
	fLowLimitX = (low > 0) ? low : 1;
	fHighLimitX = (high > 0 && high <= fData->GetNbinsX()) ? high : fData->GetNbinsX();
}

void EFractionFitter::GetXRange(Int_t minX, Int_t maxX) const
{
	minX=fLowLimitX;
	maxX=fHighLimitX;
}
void EFractionFitter::SetIntegralRangeX(Int_t low, Int_t high)
{
	fLowIntX = (low > 0) ? low : 1;
	fHighIntX = (high > 0 && high <= fData->GetNbinsX()) ? high : fData->GetNbinsX();	
	CheckConsistency();
}

void EFractionFitter::GetIntegralXRange(Int_t minX, Int_t maxX) const
{
	minX=fLowIntX;
	maxX=fHighIntX;
}
void EFractionFitter::Constrain(Int_t parm, double low, double high)
{
	assert( parm >= 0 && parm <    (Int_t) eFractionFitter->Config().ParamsSettings().size() );
	eFractionFitter->Config().ParSettings(parm).SetLimits(low,high);
}
void EFractionFitter::CheckConsistency() // incomplete but good enough for now. 
{

  if(!fData){ 
    Error("CheckConsistency", "No data histogram");
    return;
  }
  fIntegralData=0;
  for(Int_t x=fLowIntX;x<=fHighIntX; ++x){
    fIntegralData += fData->GetBinContent(x);
  }
  
  if (fNtemp < 2) {
    Error("CheckConsistency","Need at least two MC histograms");
    return;
  }
  Int_t par;
  for (par = 0; par < fNtemp; ++par) {
    TH1 *h = (TH1*)fMCs.At(par);
    if (! h) {
      Error("CheckConsistency","Nonexistent MC histogram for source #%d",par);
      return;
    }
    
    fIntegralMCs[par]=0;
    for(Int_t x=fLowIntX;x<=fHighIntX; ++x){
      Int_t bin = fData->GetBin(x);
      fIntegralMCs[par] += h->GetBinContent(bin);
    }
    
  }
}

TFitResultPtr EFractionFitter::Fit()
{
	//FCN function
	ROOT::Math::Functor fcnFunction(this,&EFractionFitter::EvaluateFCN,fNpar);
	eFractionFitter->SetFCN(static_cast<ROOT::Math::IMultiGenFunction&>(fcnFunction));

	//fit
	Bool_t status = eFractionFitter->FitFCN();
	if (!status)   Warning("Fit","Abnormal termination of minimization.");

	fFitDone=kTRUE;
	TFitResult* fr = new TFitResult(eFractionFitter->Result());
	TString name = TString::Format("EFractionFitter_result_of_%s",fData->GetName() );
	fr->SetName(name); fr->SetTitle(name);
	return TFitResultPtr(fr);

}

TGraph* EFractionFitter::Scan(unsigned int ivar, double xmin, double xmax)
{

	double* x;
	double* y;
	unsigned int nstep = 40;
	TGraph* g = new TGraph(nstep);
	bool scan = eFractionFitter->GetMinimizer()->Scan(ivar,nstep,g->GetX(),g->GetY(),xmin,xmax);
	return g;
}

// Double_t EFractionFitter::GetChisquare() const 
// {
		//finish!! 
// }


TH1* EFractionFitter::GetPlot()
{
  if(!fFitDone){
    Error("GetPlot","Fit not yet performed");
    return 0;
  }
  
  TString ts = "Fraction fit to hist: "; ts += fData->GetName();
  fPlot = (TH1*) fData->Clone(ts.Data());
  fPlot->Reset();

  TH1* ref = GetPlotWithError();
  /*
  const Double_t frac1 = eFractionFitter->Result().Parameter(0); //get template 1 fit fraction
  const Double_t frac2 = 1 - frac1; // calculate template 2 fit fraction
  const Double_t n1 = eFractionFitter->Result().Parameter(1);
  
  TH1* t1 = (TH1*)fMCs[0];
  TH1* t2 = (TH1*)fMCs[1];
  Double_t dataInt = fIntegralData;
  Double_t mcInt1 = fIntegralMCs[0];
  Double_t mcInt2 = fIntegralMCs[1];
  Double_t norm1 = dataInt/mcInt1;
  Double_t norm2 = dataInt/mcInt2;
  Int_t bin,mc;
  // Int_t minX=23;
  // Int_t maxX=71;
  */
  for(Int_t x=fLowLimitX;x<=fHighLimitX; ++x){
    int bin = fData->GetBin(x);
    fPlot->SetBinContent(bin,ref->GetBinContent(bin));
    fPlot->SetBinError(bin,ref->GetBinError(bin));
    /*
    Double_t t1binContent = (t1->GetBinContent(bin))*norm1*frac1*n1;
    Double_t t1binError = (t1->GetBinError(bin))*norm1*frac1*n1;
    
    Double_t t2binContent = (t2->GetBinContent(bin))*norm2*frac2*n1;
    Double_t t2binError = (t2->GetBinError(bin))*norm2*frac2*n1;
    
    Double_t f_x = t1binContent+t2binContent;
    fPlot->SetBinContent(bin,f_x);
    */
  }
  return fPlot;
}

TH1* EFractionFitter::GetPlotWithError()
{
  if(!fFitDone){
    Error("GetPlotWithError","Fit not yet performed");
    return 0;
  }

  TString ts = "Fraction fit to hist (with errors): "; ts += fData->GetName();
  fPlotWithError = (TH1*) fData->Clone(ts.Data());
  fPlotWithError->Reset();

  const Double_t frac1 = eFractionFitter->Result().Parameter(0); //get template 1 fit fraction
  const Double_t frac2 = 1 - frac1; // calculate template 2 fit fraction
  const Double_t n1 = eFractionFitter->Result().Parameter(1);


  Double_t dataInt = fIntegralData;
  /*  
  TH1* t1 = (TH1*)fMCs[0];
  Double_t mcInt1 = fIntegralMCs[0];
  Double_t norm1 = dataInt/mcInt1;

  TH1* t2 = (TH1*)fMCs[1];
  Double_t mcInt2 = fIntegralMCs[1];
  Double_t norm2 = dataInt/mcInt2;
  		
  // Int_t minX=23;
  // Int_t maxX=71;
  fPlotWithError->Add(t1,t2,frac1*norm1*n1,frac2*norm2*n1);
  */
  for(int i=0; i<fNtemp; i++) {
    TH1* t1 = (TH1*)fMCs[i];
    Double_t mcInt1 = fIntegralMCs[i];
    Double_t norm1 = dataInt/mcInt1;
    fPlotWithError->Add(t1,frac1*norm1*n1);
  }


  
  return fPlotWithError;
}

TH1* EFractionFitter::GetMCPrediction(Int_t parm) const
{
  Double_t dataInt = fIntegralData;


  Double_t mcInt1 = fIntegralMCs[parm];
  Double_t norm1 = dataInt/mcInt1;
  TH1* t1 = (TH1*)fMCs[parm];

  for(int x=fLowIntX; x<=fHighIntX; ++x){
    Int_t bin = fData->GetBin(x);

    Double_t t1binContent = (t1->GetBinContent(bin))*norm1;
    Double_t t1binError = (t1->GetBinError(bin))*norm1;

    ((TH1*)fPs.At(parm))->SetBinContent(bin,t1binContent);
    ((TH1*)fPs.At(parm))->SetBinError(bin,t1binError);

  }
  
  return (TH1*)fPs.At(parm);

  /*
  Double_t mcInt1 = fIntegralMCs[0];
  Double_t norm1 = dataInt/mcInt1;
  TH1* t1 = (TH1*)fMCs[0];

  Double_t mcInt2 = fIntegralMCs[1];
  Double_t norm2 = dataInt/mcInt2;
  TH1* t2 = (TH1*)fMCs[1];
  for(int x=fLowIntX; x<=fHighIntX; ++x){
    Int_t bin = fData->GetBin(x);

    Double_t t1binContent = (t1->GetBinContent(bin))*norm1;
    Double_t t1binError = (t1->GetBinError(bin))*norm1;

    Double_t t2binContent = (t2->GetBinContent(bin))*norm2;
    Double_t t2binError = (t2->GetBinError(bin))*norm2;

    ((TH1*)fPs.At(0))->SetBinContent(bin,t1binContent);
    ((TH1*)fPs.At(0))->SetBinError(bin,t1binError);

    ((TH1*)fPs.At(1))->SetBinContent(bin,t2binContent);
    ((TH1*)fPs.At(1))->SetBinError(bin,t2binError);
  }
  
  return (TH1*)fPs.At(parm);
  */
}

void EFractionFitter::GetResult(Int_t parm, Double_t& value, Double_t& error) const
{
	if(!fFitDone){
		Error("GetResult","Fit not yet performed");
		return;
	}

	value = eFractionFitter->Result().Parameter(parm);
	error = eFractionFitter->Result().Error(parm);
}



Double_t EFractionFitter::EvaluateFCN(const Double_t* par){
  Double_t f = 0;
  ComputeFCN(f,par,0);
  return f;
}

/*
void EFractionFitter::ComputeFCN(Double_t& f, const Double_t* par, Int_t flag)
{
	Int_t bin,mc;
	Int_t x;
	Double_t dataInt = fIntegralData;
	Double_t mcInt1 = fIntegralMCs[0];
	Double_t mcInt2 = fIntegralMCs[1];
	Double_t norm1 = dataInt/mcInt1;
	Double_t norm2 = dataInt/mcInt2;

	Double_t result=0;
	fNpfits=0;

	for(x=fLowLimitX; x<=fHighLimitX; ++x){
		bin = fData->GetBin(x);
		fNpfits++;
		Double_t d_x = fData->GetBinContent(bin);
		Double_t databinError = fData->GetBinError(bin);
		Double_t sigma_dx = databinError*databinError;

		TH1* t1 = (TH1*)fMCs[0];
		Double_t t1binContent = (t1->GetBinContent(bin))*norm1;
		Double_t t1binError = (t1->GetBinError(bin))*norm1;

		TH1* t2 = (TH1*)fMCs[1];
		Double_t t2binContent = (t2->GetBinContent(bin))*norm2;
		Double_t t2binError = (t2->GetBinError(bin))*norm2;

		Double_t f_x =par[1]*(t1binContent*par[0]+t2binContent*(Double_t)(1-par[0])); // 

		//Poisson Log Likelihood
		result += d_x*TMath::Log(f_x)-f_x;  

	}
		f=-result;




}
*/

void EFractionFitter::ComputeFCN(Double_t& f, const Double_t* par, Int_t flag) {

  Int_t bin,mc;
  Int_t x;
/*
  Double_t dataInt = fIntegralData;
  Double_t mcInt1 = fIntegralMCs[0];
  Double_t norm1 = dataInt/mcInt1;

  Double_t mcInt2 = fIntegralMCs[1];
  Double_t norm2 = dataInt/mcInt2;
*/
  Double_t result=0;
  fNpfits=0;

  for(x=fLowLimitX; x<=fHighLimitX; ++x){
    bin = fData->GetBin(x);
    fNpfits++;
    Double_t d_x = fData->GetBinContent(bin);
    Double_t databinError = fData->GetBinError(bin);
    Double_t sigma_dx = databinError*databinError;

    Double_t f_x =0;

    for(int i=0; i<fNtemp; i++) {
      TH1* t1 = (TH1*)fMCs[i];
      Double_t t1binContent = (t1->GetBinContent(bin));
      Double_t t1binError = (t1->GetBinError(bin));
      f_x += t1binContent*par[i];
    }
      //    TH1* t2 = (TH1*)fMCs[1];
      // Double_t t2binContent = (t2->GetBinContent(bin));
      // Double_t t2binError = (t2->GetBinError(bin));
	  
      //    Double_t f_x =(t1binContent*par[0]+t2binContent*par[1]); // 
	  
    //Poisson Log Likelihood
    result += d_x*TMath::Log(f_x)-f_x;  
	  
  }
  f=-result;
}



Int_t EFractionFitter::GetNDF() const
{
	return fNpfits-fNpar;
	
}


