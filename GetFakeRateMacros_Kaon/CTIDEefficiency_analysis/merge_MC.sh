FILES="JZ3 JZ4 JZ5 JZ6 JZ7 JZ8 JZ9 JZ10 JZ11 JZ12"
FILES="JZ3 JZ4 JZ5 JZ6 JZ7 JZ8"
for file in $FILES ; do
    echo "Merging slice ${file}"
    hadd -j -n 10000 slice_pythia_${file}.root user.hesketh.Pythia8EvtGen*${file}W*/*
#    hadd slice_sherpa_${file}.root user.hesketh.Sherpa*${file}*/*
#    hadd slice_herwigpp_${file}.root user.hesketh.Herwigpp*${file}W*/*
done

