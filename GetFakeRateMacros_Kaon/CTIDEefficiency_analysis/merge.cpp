#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include <vector>
#include <iostream>

using std::vector;
using std::cout;
using std::endl;


void MergePlot(TString directory, vector<TString> plots,   vector<TFile*> in,  vector<float> weights, TFile * out) {
  
  for(int iplot=0; iplot<plots.size(); iplot++) {
    in[0]->cd(directory);
    // cout<<in[0]->GetName()<<": "<<dirs[idir]<<"/"<<plots[iplot]<<endl;
    TH1 * tmp = (TH1D*)gDirectory->Get(plots[iplot]);
    if(!tmp) continue;
    TH1 * ref = (TH1D*)tmp->Clone();
    ref->Reset();
    
    for(int ifile=0; ifile<in.size(); ifile++){ 
      in[ifile]->cd(directory);
      TH1 * tmp = (TH1D*)gDirectory->Get(plots[iplot]);
      //  cout<<in[ifile]->GetName()<<" "<<directory<<" "<<plots[iplot]<<" "<<tmp->Integral()<<" "<<1./weights[ifile]<<" ";
      tmp->Scale(1./weights[ifile]);
      ref->Add(tmp);
      //cout<<ref->Integral()<<endl;
    }
    
    out->cd();
    out->cd(directory);
    ref->Write();
    
  }//loop over plots
  
  return;
}

void Merge(TString SAMPLE){

  cout<<"Merging "<<SAMPLE<<endl;
  
  TFile * out = new TFile("slice_pythia_"+SAMPLE+".root", "RECREATE");
  
  vector<TFile*> in;
  in.push_back( new TFile("slice_pythia_JZ3_"+SAMPLE+".root", "READ") );
  in.push_back( new TFile("slice_pythia_JZ4_"+SAMPLE+".root", "READ") );
  in.push_back( new TFile("slice_pythia_JZ5_"+SAMPLE+".root", "READ") );
  in.push_back( new TFile("slice_pythia_JZ6_"+SAMPLE+".root", "READ") );
  in.push_back( new TFile("slice_pythia_JZ7_"+SAMPLE+".root", "READ") );
  in.push_back( new TFile("slice_pythia_JZ8_"+SAMPLE+".root", "READ") );


  vector<float> weights;

  //  if(SAMPLE=="pythia") {
  weights.push_back(0.93257  ); //JZ3
  weights.push_back(118.4  );   //JZ4
  weights.push_back(3809.7 );  //JZ5
  weights.push_back(8248.0  );  //JZ6
  weights.push_back(628113  );  //JZ7
  weights.push_back(314888  );  //JZ8
    //  }
  
  vector<TString> dirs, layer_dirs, plots;
  layer_dirs.push_back("IBL");
  layer_dirs.push_back("Blayer");
  layer_dirs.push_back("layer1");
  layer_dirs.push_back("layer2");
 
  dirs.push_back("200_up_GeV");
  dirs.push_back("200_400_GeV");
  dirs.push_back("400_600_GeV");
  dirs.push_back("600_800_GeV");
  dirs.push_back("800_1000_GeV");
  dirs.push_back("1000_1200_GeV");
  dirs.push_back("1200_1400_GeV");
  dirs.push_back("1400_1600_GeV");
  dirs.push_back("1600_1800_GeV");
  dirs.push_back("1800_2000_GeV");
  dirs.push_back("2000_2200_GeV");
  dirs.push_back("2200_2400_GeV");
  dirs.push_back("2400_2600_GeV");
  dirs.push_back("2600_2800_GeV");
  dirs.push_back("2800_3000_GeV");
  //  dirs.push_back("all_GeV");


  //======================================================


  
  //top level:
  plots.push_back("jet_pT");
  plots.push_back("mu");
  out->cd();
  MergePlot("", plots, in, weights, out);


  //======================================================

  //layer level:
  plots.clear();
  plots.push_back("closure_denominator");
  plots.push_back("closure_denominator_tight");
  plots.push_back("closure_inefficiency");
  plots.push_back("closure_inefficiency_tight");
  plots.push_back("dEdx_diagonal_ntrk1");
  plots.push_back("dEdx_diagonal_ntrk2");
  plots.push_back("dEdx_diagonal_ntrk3");
  plots.push_back("dEdx_diagonal_ntrk4");
  plots.push_back("dEdx_off_diagonal_ntrk1");
  plots.push_back("dEdx_off_diagonal_ntrk2");
  plots.push_back("dEdx_off_diagonal_ntrk3");
  plots.push_back("dEdx_off_diagonal_ntrk4");
  plots.push_back("dEdx_shared_truth");
  for(int ilay=0; ilay<layer_dirs.size(); ilay++) {
    TString directory = layer_dirs[ilay];
    out->cd();
    gDirectory->mkdir(directory);
    MergePlot(directory, plots, in, weights, out);
  }
 
  //======================================================

  plots.clear();
  for(int i=1; i<4; i++) {
    TString itrk=""; itrk+=i;
    plots.push_back("dEdx_out_ntrk"+itrk);
    plots.push_back("dEdx_out1_ntrk"+itrk);
    plots.push_back("dEdx_out2_ntrk"+itrk);
    plots.push_back("dEdx_in_ntrk"+itrk);
    plots.push_back("dEdx_in1_ntrk"+itrk);
    plots.push_back("dEdx_in2_ntrk"+itrk);

    plots.push_back("dEdx_out_ntrk"+itrk+"_calib");
    plots.push_back("dEdx_out1_ntrk"+itrk+"_calib");
    plots.push_back("dEdx_out2_ntrk"+itrk+"_calib");
    plots.push_back("dEdx_in_ntrk"+itrk+"_calib");
    plots.push_back("dEdx_in1_ntrk"+itrk+"_calib");
    plots.push_back("dEdx_in2_ntrk"+itrk+"_calib");

  }
  plots.push_back("dEdx_eff_truth");
  plots.push_back("dEdx_ineff_truth");
  
  for(int ilay=0; ilay<layer_dirs.size(); ilay++) {
    for(int idir=0; idir<dirs.size(); idir++) {
      TString directory = layer_dirs[ilay]+"/"+dirs[idir];
      out->cd();
      gDirectory->mkdir(directory);
      MergePlot(directory, plots, in, weights, out);
  
    }//loop over directories
  }
  
  
  

  out->Close();
  return;
}



int main() {

  Merge("MC16");
  Merge("MC16c");
  //  Merge("sherpa");
  // Merge("herwigpp");
  
  return 1;
}


