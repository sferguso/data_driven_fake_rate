//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Oct 11 21:40:24 2018 by ROOT version 6.10/04
// from TTree analysis/My analysis ntuple
// found on file: /eos/user/s/sferguso/KaonAnalysisData/WithLooseTrackSelection/data17/data17.root
//////////////////////////////////////////////////////////

#ifndef GetFakeRate_Kaon_h
#define GetFakeRate_Kaon_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class GetFakeRate_Kaon {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNumber;
   Int_t           EventNumber;
   Float_t         AvgMu;
   Int_t           Ntracks;
   vector<int>     *TrackCharge;
   vector<float>   *TrackpT;
   vector<float>   *track_eta;
   vector<float>   *track_phi;
   vector<float>   *track_E;
   vector<float>   *track_d0;
   vector<float>   *track_sinTheta;
   vector<int>     *track_Npixelhits;
   vector<int>     *track_NSCThits;
   vector<int>     *track_NTRThits;
   vector<int>     *pdgId;
   vector<float>   *track_TruthMatchProb;
   vector<float>   *kaon_mpipi;
   vector<float>   *kaon_pipipT;
   vector<float>   *kaon_pipiDeltaR;
   vector<float>   *kaon_pi1d0;
   vector<float>   *kaon_pi1phi;
   vector<float>   *kaon_pi1eta;
   vector<float>   *kaon_pi1pT;
   vector<float>   *kaon_pi1sinTheta;
   vector<float>   *kaon_pi2d0;
   vector<float>   *kaon_pi2phi;
   vector<float>   *kaon_pi2eta;
   vector<float>   *kaon_pi2pT;
   vector<float>   *kaon_pi2sinTheta;
   vector<float>   *kaon_xylength;
   vector<float>   *kaon_cosThetaStar;
   vector<float>   *kaon_pipivertex_x;
   vector<float>   *kaon_pipivertex_y;
   vector<float>   *kaon_pipivertex_z;
   vector<int>     *kaon_pi2Npixelhits;
   vector<int>     *kaon_pi2NSCThits;
   vector<int>     *kaon_pi2NTRThits;
   vector<float>   *kaon_pi2TruthMatchProb;
   vector<int>     *kaon_pi1Npixelhits;
   vector<int>     *kaon_pi1NSCThits;
   vector<int>     *kaon_pi1NTRThits;
   vector<float>   *kaon_pi1TruthMatchProb;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_AvgMu;   //!
   TBranch        *b_Ntracks;   //!
   TBranch        *b_TrackCharge;   //!
   TBranch        *b_TrackpT;   //!
   TBranch        *b_track_eta;   //!
   TBranch        *b_track_phi;   //!
   TBranch        *b_track_E;   //!
   TBranch        *b_track_d0;   //!
   TBranch        *b_track_sinTheta;   //!
   TBranch        *b_track_Npixelhits;   //!
   TBranch        *b_track_NSCThits;   //!
   TBranch        *b_track_NTRThits;   //!
   TBranch        *b_pdgId;   //!
   TBranch        *b_track_TruthMatchProb;   //!
   TBranch        *b_kaon_mpipi;   //!
   TBranch        *b_kaon_pipipT;   //!
   TBranch        *b_kaon_pipiDeltaR;   //!
   TBranch        *b_kaon_pi1d0;   //!
   TBranch        *b_kaon_pi1phi;   //!
   TBranch        *b_kaon_pi1eta;   //!
   TBranch        *b_kaon_pi1pT;   //!
   TBranch        *b_kaon_pi1sinTheta;   //!
   TBranch        *b_kaon_pi2d0;   //!
   TBranch        *b_kaon_pi2phi;   //!
   TBranch        *b_kaon_pi2eta;   //!
   TBranch        *b_kaon_pi2pT;   //!
   TBranch        *b_kaon_pi2sinTheta;   //!
   TBranch        *b_kaon_xylength;   //!
   TBranch        *b_kaon_cosThetaStar;   //!
   TBranch        *b_kaon_pipivertex_x;   //!
   TBranch        *b_kaon_pipivertex_y;   //!
   TBranch        *b_kaon_pipivertex_z;   //!
   TBranch        *b_kaon_pi2Npixelhits;   //!
   TBranch        *b_kaon_pi2NSCThits;   //!
   TBranch        *b_kaon_pi2NTRThits;   //!
   TBranch        *b_kaon_pi2TruthMatchProb;   //!
   TBranch        *b_kaon_pi1Npixelhits;   //!
   TBranch        *b_kaon_pi1NSCThits;   //!
   TBranch        *b_kaon_pi1NTRThits;   //!
   TBranch        *b_kaon_pi1TruthMatchProb;   //!

   GetFakeRate_Kaon(TTree *tree=0);
   virtual ~GetFakeRate_Kaon();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef GetFakeRate_Kaon_cxx
GetFakeRate_Kaon::GetFakeRate_Kaon(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/user/s/sferguso/KaonAnalysisData/WithLooseTrackSelection/MC16/MC16_small.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/user/s/sferguso/KaonAnalysisData/WithLooseTrackSelection/MC16/MC16_small.root");
      }
      f->GetObject("analysis",tree);

   }
   Init(tree);
}

GetFakeRate_Kaon::~GetFakeRate_Kaon()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t GetFakeRate_Kaon::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t GetFakeRate_Kaon::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void GetFakeRate_Kaon::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TrackCharge = 0;
   TrackpT = 0;
   track_eta = 0;
   track_phi = 0;
   track_E = 0;
   track_d0 = 0;
   track_sinTheta = 0;
   track_Npixelhits = 0;
   track_NSCThits = 0;
   track_NTRThits = 0;
   pdgId = 0;
   track_TruthMatchProb = 0;
   kaon_mpipi = 0;
   kaon_pipipT = 0;
   kaon_pipiDeltaR = 0;
   kaon_pi1d0 = 0;
   kaon_pi1phi = 0;
   kaon_pi1eta = 0;
   kaon_pi1pT = 0;
   kaon_pi1sinTheta = 0;
   kaon_pi2d0 = 0;
   kaon_pi2phi = 0;
   kaon_pi2eta = 0;
   kaon_pi2pT = 0;
   kaon_pi2sinTheta = 0;
   kaon_xylength = 0;
   kaon_cosThetaStar = 0;
   kaon_pipivertex_x = 0;
   kaon_pipivertex_y = 0;
   kaon_pipivertex_z = 0;
   kaon_pi2Npixelhits = 0;
   kaon_pi2NSCThits = 0;
   kaon_pi2NTRThits = 0;
   kaon_pi2TruthMatchProb = 0;
   kaon_pi1Npixelhits = 0;
   kaon_pi1NSCThits = 0;
   kaon_pi1NTRThits = 0;
   kaon_pi1TruthMatchProb = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("AvgMu", &AvgMu, &b_AvgMu);
   fChain->SetBranchAddress("Ntracks", &Ntracks, &b_Ntracks);
   fChain->SetBranchAddress("TrackCharge", &TrackCharge, &b_TrackCharge);
   fChain->SetBranchAddress("TrackpT", &TrackpT, &b_TrackpT);
   fChain->SetBranchAddress("track_eta", &track_eta, &b_track_eta);
   fChain->SetBranchAddress("track_phi", &track_phi, &b_track_phi);
   fChain->SetBranchAddress("track_E", &track_E, &b_track_E);
   fChain->SetBranchAddress("track_d0", &track_d0, &b_track_d0);
   fChain->SetBranchAddress("track_sinTheta", &track_sinTheta, &b_track_sinTheta);
   fChain->SetBranchAddress("track_Npixelhits", &track_Npixelhits, &b_track_Npixelhits);
   fChain->SetBranchAddress("track_NSCThits", &track_NSCThits, &b_track_NSCThits);
   fChain->SetBranchAddress("track_NTRThits", &track_NTRThits, &b_track_NTRThits);
   fChain->SetBranchAddress("pdgId", &pdgId, &b_pdgId);
   fChain->SetBranchAddress("track_TruthMatchProb", &track_TruthMatchProb, &b_track_TruthMatchProb);
   fChain->SetBranchAddress("kaon_mpipi", &kaon_mpipi, &b_kaon_mpipi);
   fChain->SetBranchAddress("kaon_pipipT", &kaon_pipipT, &b_kaon_pipipT);
   fChain->SetBranchAddress("kaon_pipiDeltaR", &kaon_pipiDeltaR, &b_kaon_pipiDeltaR);
   fChain->SetBranchAddress("kaon_pi1d0", &kaon_pi1d0, &b_kaon_pi1d0);
   fChain->SetBranchAddress("kaon_pi1phi", &kaon_pi1phi, &b_kaon_pi1phi);
   fChain->SetBranchAddress("kaon_pi1eta", &kaon_pi1eta, &b_kaon_pi1eta);
   fChain->SetBranchAddress("kaon_pi1pT", &kaon_pi1pT, &b_kaon_pi1pT);
   fChain->SetBranchAddress("kaon_pi1sinTheta", &kaon_pi1sinTheta, &b_kaon_pi1sinTheta);
   fChain->SetBranchAddress("kaon_pi2d0", &kaon_pi2d0, &b_kaon_pi2d0);
   fChain->SetBranchAddress("kaon_pi2phi", &kaon_pi2phi, &b_kaon_pi2phi);
   fChain->SetBranchAddress("kaon_pi2eta", &kaon_pi2eta, &b_kaon_pi2eta);
   fChain->SetBranchAddress("kaon_pi2pT", &kaon_pi2pT, &b_kaon_pi2pT);
   fChain->SetBranchAddress("kaon_pi2sinTheta", &kaon_pi2sinTheta, &b_kaon_pi2sinTheta);
   fChain->SetBranchAddress("kaon_xylength", &kaon_xylength, &b_kaon_xylength);
   fChain->SetBranchAddress("kaon_cosThetaStar", &kaon_cosThetaStar, &b_kaon_cosThetaStar);
   fChain->SetBranchAddress("kaon_pipivertex_x", &kaon_pipivertex_x, &b_kaon_pipivertex_x);
   fChain->SetBranchAddress("kaon_pipivertex_y", &kaon_pipivertex_y, &b_kaon_pipivertex_y);
   fChain->SetBranchAddress("kaon_pipivertex_z", &kaon_pipivertex_z, &b_kaon_pipivertex_z);
   fChain->SetBranchAddress("kaon_pi2Npixelhits", &kaon_pi2Npixelhits, &b_kaon_pi2Npixelhits);
   fChain->SetBranchAddress("kaon_pi2NSCThits", &kaon_pi2NSCThits, &b_kaon_pi2NSCThits);
   fChain->SetBranchAddress("kaon_pi2NTRThits", &kaon_pi2NTRThits, &b_kaon_pi2NTRThits);
   fChain->SetBranchAddress("kaon_pi2TruthMatchProb", &kaon_pi2TruthMatchProb, &b_kaon_pi2TruthMatchProb);
   fChain->SetBranchAddress("kaon_pi1Npixelhits", &kaon_pi1Npixelhits, &b_kaon_pi1Npixelhits);
   fChain->SetBranchAddress("kaon_pi1NSCThits", &kaon_pi1NSCThits, &b_kaon_pi1NSCThits);
   fChain->SetBranchAddress("kaon_pi1NTRThits", &kaon_pi1NTRThits, &b_kaon_pi1NTRThits);
   fChain->SetBranchAddress("kaon_pi1TruthMatchProb", &kaon_pi1TruthMatchProb, &b_kaon_pi1TruthMatchProb);
   Notify();
}

Bool_t GetFakeRate_Kaon::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void GetFakeRate_Kaon::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t GetFakeRate_Kaon::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef GetFakeRate_Kaon_cxx
