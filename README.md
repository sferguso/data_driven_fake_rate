KaonAnalysis Directory has Eventloop to produce tuples 

To set up: 
  
1. make a new working directory
2. mkdir build run 
3. use git clone --recursive to copy to directory

then: 

     setupATLAS
     cd build
     lsetup asetup
     asetup AnalysisBase,21.2.
     cmake ../data_driven_fake_rate
     make
     source x*/setup.sh

  On every login: 

     setupATLAS
     cd build
     asetup --restore
     source x*/setup.sh

To run a test job: 

     cd run
     root -b -q '$ROOTCOREDIR/scripts/load_packages.C' '../data_driven_fake_rate/KaonAnalysis/util/RunTestJob.cxx ("submitDir")'

To run a grid job from a clean shell: 

     cd build
     setupATLAS
     lsetup panda
     asetup --restore
     source x*/setup.sh
     cd ../run
     root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' '../data_driven_fake_rate/KaonAnalysis/util/RunGridJob.cxx ("submitDir")'



GetFakeRateMacros_Kaon directory has macro for creating plots and getting fake rate. GetFakeRateMacros_Kaon/README has instructions


