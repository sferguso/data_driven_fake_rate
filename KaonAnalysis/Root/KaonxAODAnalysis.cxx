#include <AsgTools/MessageCheck.h>
#include <KaonAnalysis/KaonxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <TSystem.h>
#include <xAODTracking/TrackParticleContainer.h>
#include "PathResolver/PathResolver.h"
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>


TVector3 kvertex(const xAOD::TrackParticle* trk1, const xAOD::TrackParticle* trk2) {
  // This does a crude 2D vertex of two tracks, assuming straight line. Z is junk but not used.
  double d01 = trk1->d0();
  double phi1 = trk1->phi();
  double d02 = trk2->d0();
  double phi2 = trk2->phi();

  double den= sin(phi1)*cos(phi2)-cos(phi1)*sin(phi2);
  double  y= (-d01*sin(phi2) + d02*sin(phi1))/den;
  double x = (-d01 + y*cos(phi1))/sin(phi1);
  double z = (trk1->z0() + trk2->z0())/2.; // just give mean z for now

  TVector3 result(x,y,z);
  return result;
} //End of vertex


TLorentzVector kvertex4v(TVector3 vertex,const xAOD::TrackParticle* trk,double mass) {
  // This finds the 4-vector extrapolated to a vertex on the track
  // Its very crude but good enough for a cm or two of a high-pt track
  const double b = 2.0; // Assume 2T field!!!!

  double phi = trk->phi();
  double r = trk->pt()/0.4/b;
  double dist = fabs(vertex.x()*cos(phi) + vertex.y()*sin(phi));
  double dphi = -dist/r * trk->charge();
  phi += dphi;

  TLorentzVector result;
  result.SetPtEtaPhiM(trk->pt(),trk->eta(),phi,mass);
  return result;
} //End of vertex4v



KaonxAODAnalysis :: KaonxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator), 
    m_grl ("GoodRunsListSelectionTool/grl", this), 
    m_selTool("InDet::InDetTrackSelectionTool/TrackSelectionTool", this)

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.


}



StatusCode KaonxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  //ANA_MSG_INFO ("in initialize");

   // GRL
  std::vector<std::string> vecStringGRL;
  //2016
//  vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
  //2017
  vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"));
//  vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180618/data17_13TeV.periodAllYear_DetStatus-v97-pro21-17_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"));
  ANA_CHECK( m_grl.setProperty( "GoodRunsListVec", vecStringGRL) );
  ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl.initialize());

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree *MyTree = tree ("analysis");
  MyTree->Branch("RunNumber",&tRunNumber);
  MyTree->Branch("EventNumber",&tEventNumber);
  MyTree->Branch("AvgMu",&tAvgMu);
  MyTree->Branch("Ntracks",&tNtracks);
  ttrack_charge = new std::vector<int>();
  MyTree->Branch("TrackCharge",&ttrack_charge);
  ttrack_pT = new std::vector<float>();
  MyTree->Branch("TrackpT",&ttrack_pT);
  ttrack_eta = new std::vector<float>();
  MyTree->Branch("track_eta",&ttrack_eta);
  ttrack_phi = new std::vector<float>();
  MyTree->Branch("track_phi",&ttrack_phi);
  ttrack_E = new std::vector<float>();
  MyTree->Branch("track_E",&ttrack_E);
  ttrack_d0 = new std::vector<float>();
  MyTree->Branch("track_d0",&ttrack_d0);
  ttrack_sinTheta = new std::vector<float>();
  MyTree->Branch("track_sinTheta",&ttrack_sinTheta);
  ttrack_Npixelhits = new std::vector<int>();
  MyTree->Branch("track_Npixelhits",&ttrack_Npixelhits);
  ttrack_NSCThits = new std::vector<int>();
  MyTree->Branch("track_NSCThits",&ttrack_NSCThits);
  ttrack_NTRThits = new std::vector<int>();
  MyTree->Branch("track_NTRThits",&ttrack_NTRThits);
  tpdgId = new std::vector<int>();
  MyTree->Branch("pdgId",&tpdgId);
  ttrack_TruthMatchProb = new std::vector<float>();
  MyTree->Branch("track_TruthMatchProb",&ttrack_TruthMatchProb);

//pion and reconstructed kaon info: 
  tkaon_mpipi = new std::vector<float>();
  MyTree->Branch("kaon_mpipi",&tkaon_mpipi);
  tkaon_pipipT = new std::vector<float>();
  MyTree->Branch("kaon_pipipT",&tkaon_pipipT);
  tkaon_pipiDeltaR = new std::vector<float>();
  MyTree->Branch("kaon_pipiDeltaR",&tkaon_pipiDeltaR);
  tkaon_pi1d0 = new std::vector<float>();
  MyTree->Branch("kaon_pi1d0",&tkaon_pi1d0);
  tkaon_pi1phi = new std::vector<float>();
  MyTree->Branch("kaon_pi1phi",&tkaon_pi1phi);
  tkaon_pi1eta = new std::vector<float>();
  MyTree->Branch("kaon_pi1eta",&tkaon_pi1eta);
  tkaon_pi1pT = new std::vector<float>();
  MyTree->Branch("kaon_pi1pT",&tkaon_pi1pT);
  tkaon_pi1sinTheta = new std::vector<float>();
  MyTree->Branch("kaon_pi1sinTheta",&tkaon_pi1sinTheta);
  tkaon_pi2d0 = new std::vector<float>();
  MyTree->Branch("kaon_pi2d0",&tkaon_pi2d0);
  tkaon_pi2phi = new std::vector<float>();
  MyTree->Branch("kaon_pi2phi",&tkaon_pi2phi);
  tkaon_pi2eta = new std::vector<float>();
  MyTree->Branch("kaon_pi2eta",&tkaon_pi2eta);
  tkaon_pi2pT = new std::vector<float>();
  MyTree->Branch("kaon_pi2pT",&tkaon_pi2pT);
  tkaon_pi2sinTheta = new std::vector<float>();
  MyTree->Branch("kaon_pi2sinTheta",&tkaon_pi2sinTheta);
  tkaon_xylength = new std::vector<float>();
  MyTree->Branch("kaon_xylength",&tkaon_xylength);
  tkaon_cosThetaStar  = new std::vector<float>();
  MyTree->Branch("kaon_cosThetaStar",&tkaon_cosThetaStar);
  tkaon_pipivertex_x = new std::vector<float>();
  MyTree->Branch("kaon_pipivertex_x",&tkaon_pipivertex_x);
  tkaon_pipivertex_y = new std::vector<float>();
  MyTree->Branch("kaon_pipivertex_y",&tkaon_pipivertex_y);
  tkaon_pipivertex_z = new std::vector<float>();
  MyTree->Branch("kaon_pipivertex_z",&tkaon_pipivertex_z);
  tkaon_pi2Npixelhits = new std::vector<int>();
  MyTree->Branch("kaon_pi2Npixelhits",&tkaon_pi2Npixelhits);
  tkaon_pi2NSCThits = new std::vector<int>();
  MyTree->Branch("kaon_pi2NSCThits",&tkaon_pi2NSCThits);
  tkaon_pi2NTRThits = new std::vector<int>();
  MyTree->Branch("kaon_pi2NTRThits",&tkaon_pi2NTRThits);
  tkaon_pi2TruthMatchProb = new std::vector<float>();
  MyTree->Branch("kaon_pi2TruthMatchProb",&tkaon_pi2TruthMatchProb);
  tkaon_pi1Npixelhits = new std::vector<int>();
  MyTree->Branch("kaon_pi1Npixelhits",&tkaon_pi1Npixelhits);
  tkaon_pi1NSCThits = new std::vector<int>();
  MyTree->Branch("kaon_pi1NSCThits",&tkaon_pi1NSCThits);
  tkaon_pi1NTRThits = new std::vector<int>();
  MyTree->Branch("kaon_pi1NTRThits",&tkaon_pi1NTRThits);
  tkaon_pi1TruthMatchProb = new std::vector<float>();
  MyTree->Branch("kaon_pi1TruthMatchProb",&tkaon_pi1TruthMatchProb);

//MC truth values: 
  ttruth_isLinked = new std::vector<bool>(); //if the track has a truth link
  MyTree->Branch("truth_isLinked",&ttruth_isLinked);
  ttruth_isKshortpion1 = new std::vector<bool>(); //if the track is truth matched to a pion from a kshort
  MyTree->Branch("truth_isKshortpion1",&ttruth_isKshortpion1);
  ttruth_isKshortpion2 = new std::vector<bool>(); //if the track is truth matched to a pion from a kshort
  MyTree->Branch("truth_isKshortpion2",&ttruth_isKshortpion2);
  ttruth_pdgId1 = new std::vector<int>();
  MyTree->Branch("truth_pdgId1",&ttruth_pdgId1);
  ttruth_pdgId2 = new std::vector<int>();
  MyTree->Branch("truth_pdgId2",&ttruth_pdgId2);
  ttruth_barcode1 = new std::vector<int>();
  MyTree->Branch("truth_barcode1",&ttruth_barcode1);
  ttruth_barcode2 = new std::vector<int>();
  MyTree->Branch("truth_barcode2",&ttruth_barcode2);


  ANA_CHECK (m_selTool.setProperty("CutLevel", "LoosePrimary"));
  ANA_CHECK (m_selTool.initialize());

  ANA_CHECK (book (TH1D("hkmass","kaon mass",100 , 400, 600)));
  ANA_CHECK (book (TH1D("hkmass_truth"," kaon mass in all truth matched events",100 , 400, 600)));
  ANA_CHECK (book (TH1D("hkmass_tke"," kaon mass in truth matched k-short events",100 , 400, 600)));
  ANA_CHECK (book (TH1D("hkmass_NOTtke"," kaon mass in truth matched NOT k-short events",100 , 400, 600)));
//tke means truthkaonevent
  ANA_CHECK (book (TH1D("npix_MCreal","truth matched number of real pixel hits",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_MCfake","truth matched number of fake pixel hits",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_side","number of pixel hits in  sideband region",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_sig","number of pixel hits in signal region",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_sig_test","number of pixel hits in signal test region",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_side_test","number of pixel hits in sideband test region",10,-0.5,9.5)));

  ANA_CHECK (book (TH1D("npix_sig_tke","number of pixel hits in signal region (truth matched events, only k-short pions)",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_sig_NOTtke","number of pixel hits in signal region (truth matched events, NOT k-short pions)",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_side_tke","number of pixel hits in sideband region (truth matched events, only k-short pions)",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_side_NOTtke","number of pixel hits in sideband region (truth matched events, NOT k-short pions)",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_tke_all","number of pixel hits in all regions (truth matched events, only k-short pions)",10,-0.5,9.5)));
  ANA_CHECK (book (TH1D("npix_NOTtke_all","number of pixel hits in all regions (truth matched events, NOT k-short pions)",10,-0.5,9.5)));

  ANA_CHECK (book (TH1D("nSCT_MCreal","truth matched number of real SCT hits",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_MCfake","truth matched number of fake SCT hits",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_side","number of SCT hits in  sideband region",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_sig","number of SCT hits in signal region",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_sig_test","number of SCT hits in signal test region",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_side_test","number of SCT hits in sideband test region",18,-0.5,17.5)));

  ANA_CHECK (book (TH1D("nSCT_sig_tke","number of SCT hits in signal region (truth matched events, only k-short pions)",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_sig_NOTtke","number of SCT hits in signal region (truth matched events, NOT k-short pions)",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_side_tke","number of SCT hits in sideband region (truth matched events, only k-short pions)",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_side_NOTtke","number of SCT hits in sideband region (truth matched events, NOT k-short pions)",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_tke_all","number of SCT hits in all regions (truth matched events, only k-short pions)",18,-0.5,17.5)));
  ANA_CHECK (book (TH1D("nSCT_NOTtke_all","number of SCT hits in all regions (truth matched events, NOT k-short pions)",18,-0.5,17.5)));

  ANA_CHECK (book (TH1D("nTRT_MCreal","truth matched number of real TRT hits",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_MCfake","truth matched number of fake TRT hits",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_side","number of TRT hits in  sideband region",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_sig","number of TRT hits in signal region",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_sig_test","number of TRT hits in signal test region",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_side_test","number of TRT hits in sideband test region",70,-0.5,69.5)));

  ANA_CHECK (book (TH1D("nTRT_sig_tke","number of TRT hits in signal region (truth matched events, only k-short pions)",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_sig_NOTtke","number of TRT hits in signal region (truth matched events, NOT k-short pions)",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_side_tke","number of TRT hits in sideband region (truth matched events, only k-short pions)",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_side_NOTtke","number of TRT hits in sideband region (truth matched events, NOT k-short pions)",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_tke_all","number of TRT hits in all regions (truth matched events, only k-short pions)",70,-0.5,69.5)));
  ANA_CHECK (book (TH1D("nTRT_NOTtke_all","number of TRT hits in all regions (truth matched events, NOT k-short pions)",70,-0.5,69.5)));

  ANA_CHECK (book (TH1D("prob","truth match probability",100,0,1)));
  ANA_CHECK (book (TH1D("prob_sig","truth match probability - signal region",100,0,1)));
  ANA_CHECK (book (TH1D("prob_sideband","truth match probability - sideband region",100,0,1)));

  ANA_CHECK (book (TH2D("d01vkmass","+pion d0 vs K0 mass", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("d02vkmass","-pion d0 vs K0 mass", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("xylengthvkmass","xylength vs K0 mass",100, 400, 600, 600, -30, 30)));
  ANA_CHECK (book (TH2D("cosThstvkmass","cos(ThetaStar) vs K0 mass",100, 400, 600, 100, -1 ,1)));
  ANA_CHECK (book (TH2D("DeltaRvskmass","Delta R vs K0 mass",100, 400, 600, 100, 0, 0.42)));
  ANA_CHECK (book (TH2D("kpTvskmass","Kaon pT vs Kaon mass",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi1pTvskmass","+pion pT vs Kaon mass",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi2pTvskmass","-pion pT vs Kaon mass",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pixhitsvskmass","pixel hits vs Kaon mass",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("scthitsvskmass","SCT hits vs Kaon mass",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("trthitsvskmass","TRT hits vs Kaon mass",100,400,600,10,-0.5,9.5)));

  ANA_CHECK (book (TH2D("d01vkmass_tke","+pion d0 vs K0 mass (truth matched events, only k-short pions)", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("d02vkmass_tke","-pion d0 vs K0 mass (truth matched events, only k-short pions)", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("xylengthvkmass_tke","xylength vs K0 mass (truth matched events, only k-short pions)",100, 400, 600, 600, -30, 30)));
  ANA_CHECK (book (TH2D("cosThstvkmass_tke","cos(ThetaStar) vs K0 mass (truth matched events, only k-short pions)",100, 400, 600, 100, -1 ,1)));
  ANA_CHECK (book (TH2D("DeltaRvskmass_tke","Delta R vs K0 mass (truth matched events, only k-short pions)",100, 400, 600, 100, 0, 0.42)));
  ANA_CHECK (book (TH2D("kpTvskmass_tke","Kaon pT vs Kaon mass (truth matched events, only k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi1pTvskmass_tke","+pion pT vs Kaon mass (truth matched events, only k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi2pTvskmass_tke","-pion pT vs Kaon mass (truth matched events, only k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pixhitsvskmass_tke","pixel hits vs Kaon mass (truth matched events, only k-short pions)",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("scthitsvskmass_tke","SCT hits vs Kaon mass (truth matched events, only k-short pions)",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("trthitsvskmass_tke","TRT hits vs Kaon mass(truth matched events, only k-short pions)",100,400,600,10,-0.5,9.5)));

  ANA_CHECK (book (TH2D("d01vkmass_NOTtke","+pion d0 vs K0 mass (truth matched events, NOT k-short pions)", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("d02vkmass_NOTtke","-pion d0 vs K0 mass (truth matched events, NOT k-short pions)", 100, 400, 600, 200, -1, 1)));
  ANA_CHECK (book (TH2D("xylengthvkmass_NOTtke","xylength vs K0 mass (truth matched events, NOT k-short pions)",100, 400, 600, 600, -30, 30)));
  ANA_CHECK (book (TH2D("cosThstvkmass_NOTtke","cos(ThetaStar) vs K0 mass (truth matched events, NOT k-short pions)",100, 400, 600, 100, -1 ,1)));
  ANA_CHECK (book (TH2D("DeltaRvskmass_NOTtke","Delta R vs K0 mass (truth matched events, NOT k-short pions)",100, 400, 600, 100, 0, 0.42)));
  ANA_CHECK (book (TH2D("kpTvskmass_NOTtke","Kaon pT vs Kaon mass (truth matched events, NOT k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi1pTvskmass_NOTtke","+pion pT vs Kaon mass (truth matched events, NOT k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pi2pTvskmass_NOTtke","-pion pT vs Kaon mass (truth matched events, NOT k-short pions)",100,400,600,100,0,100000)));
  ANA_CHECK (book (TH2D("pixhitsvskmass_NOTtke","pixel hits vs Kaon mass (truth matched events, NOT k-short pions)",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("scthitsvskmass_NOTtke","SCT hits vs Kaon mass (truth matched events, NOT k-short pions)",100,400,600,10,-0.5,9.5)));
  ANA_CHECK (book (TH2D("trthitsvskmass_NOTtke","TRT hits vs Kaon mass(truth matched events, NOT k-short pions)",100,400,600,10,-0.5,9.5)));

  ANA_CHECK (book (TH1D("truthKaonpT","truthKaonpT",100,0,20000)));
  ANA_CHECK (book (TH1D("truthKaonDeltaR","truthKaonDeltaR",100,-0.1,1)));
  return StatusCode::SUCCESS;
}



StatusCode KaonxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  m_numevents++;
  if (m_numevents % 10000 ==0)  ANA_MSG_INFO ("in execute, m_numevents = " << m_numevents);

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve( eventInfo, "EventInfo"));  
  
  // print out run and event number from retrieved object
  bool printinfo = false;
//  if (m_numevents % 10000 ==0){
//  printinfo = true;
//  ANA_MSG_INFO ("in execute, m_numevents = " << m_numevents << " runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
//  }

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true; // can do something with this later
  }

  // if data check if event passes GRL
  if (!isMC) { // it's data!
/*    if (!m_grl->passRunLB(*eventInfo)) {
      if (printinfo) ANA_MSG_INFO ("drop event: GRL");
      return StatusCode::SUCCESS; // go to next event
    } */
  } // end if not MC

  if (printinfo)  ANA_MSG_INFO ("keep event: GRL");

  ///--------------------------------------
  /// FILL VARIABLES
  ///--------------------------------------

  tRunNumber = eventInfo->runNumber();
  tEventNumber = (Int_t)eventInfo->eventNumber();

  ///--------------------------------------
  /// loop over the tracks in the container
  ///--------------------------------------

  const xAOD::TrackParticleContainer* Tracks = nullptr;
  ANA_CHECK(evtStore()->retrieve( Tracks, "InDetTrackParticles" ));

  ttrack_charge->clear();
  ttrack_pT->clear();
  ttrack_eta->clear();
  ttrack_phi->clear();
  ttrack_E->clear();
  ttrack_d0->clear();
  ttrack_sinTheta->clear();
  ttrack_Npixelhits->clear();
  ttrack_NSCThits->clear();
  ttrack_NTRThits->clear();
  ttrack_TruthMatchProb->clear();
  tkaon_mpipi->clear();
  tkaon_pipipT->clear();
  tkaon_pipiDeltaR->clear();
  tkaon_pi1d0->clear();
  tkaon_pi1phi->clear();
  tkaon_pi1eta->clear();
  tkaon_pi1pT->clear();
  tkaon_pi1sinTheta->clear();
  tkaon_pi2d0->clear();
  tkaon_pi2phi->clear();
  tkaon_pi2eta->clear();
  tkaon_pi2pT->clear();
  tkaon_pi2sinTheta->clear();
  tkaon_pipivertex_x->clear();
  tkaon_pipivertex_y->clear();
  tkaon_pipivertex_z->clear();
  tkaon_xylength->clear();
  tkaon_cosThetaStar->clear();
  tkaon_pi2TruthMatchProb->clear();
  tkaon_pi1TruthMatchProb->clear();
  tkaon_pi2Npixelhits->clear();
  tkaon_pi2NSCThits->clear();
  tkaon_pi2NTRThits->clear();
  tkaon_pi1Npixelhits->clear();
  tkaon_pi1NSCThits->clear();
  tkaon_pi1NTRThits->clear();

  ttruth_isLinked->clear();
  ttruth_pdgId1->clear();
  ttruth_pdgId2->clear();
  ttruth_barcode1->clear();
  ttruth_barcode2->clear();
  ttruth_isKshortpion1->clear();
  ttruth_isKshortpion2->clear();

  tNtracks = 0;
  for( auto track : *Tracks ) {
  tNtracks++;

  //track selection
  if ( !m_selTool->accept(track)) {
    if (printinfo) ANA_MSG_INFO ("trackParticle fails selection" << "\n");
  continue;
  }

  if ( m_selTool->accept(track)) {
  if (printinfo) ANA_MSG_INFO ("trackParticle passes selection" << "\n");
  }

  uint8_t nPixel=0; //, nPixelSplitHits=0;
  uint8_t nSCT=0; //, nSCTHoles=0;
  uint8_t nTRT=0;
  track->summaryValue( nPixel, xAOD::numberOfPixelHits );
  //track->summaryValue( nPixelSplitHits, xAOD::numberOfPixelSplitHits );
  track->summaryValue( nSCT, xAOD::numberOfSCTHits );
  //track->summaryValue( nSCTHoles, xAOD::numberOfSCTHoles );
  track->summaryValue( nTRT, xAOD::numberOfTRTHits );
  ttrack_charge->push_back(track->charge());
  ttrack_pT->push_back(track->pt());
  ttrack_eta->push_back(track->eta());
  ttrack_phi->push_back(track->phi());
  ttrack_E->push_back(track->e());
  ttrack_d0->push_back(track->d0());
  ttrack_sinTheta->push_back(sin(track->theta()));
  ttrack_Npixelhits->push_back((int)nPixel);
  ttrack_NSCThits->push_back((int)nSCT);
  ttrack_NTRThits->push_back((int)nTRT);
  if(isMC){
	ttrack_TruthMatchProb->push_back(track -> auxdata< float >("truthMatchProbability"));
  }//end isMC
  }


  //---------------
  //BEGIN K0 SEARCH
  //---------------

///////
//CUTS for histograms
double_t DeltaRmax = 0.4; //cut out above this
double_t d0 = .15; //cut out below this
double_t xylengthmin = 3.3; //3.3 cm is the min radius of the IBL, 5.0 is min radius of pixel detector, 30 is min of SCT, 51 is min of TRT
double_t xylengthmax = 30.0;
double_t xylengthmax_pix = 13.0;
double_t xylengthmax_endcap = 30.0;
double_t cosThstcutmax = 0.8; //cut out over this
double_t kpTcutmax = 200000; //cut out over this
double_t kpTcutmin = 3000; //cut out below this
double_t eta_barrel_max = 2.2;
double_t eta_endcap_min = 1.6;
double_t eta_endcap_max = 2.7;

// Signal and sideband region definitions:
double_t mean_guess = 498; //from fit
double_t sigma_guess = 10; //from fit

//Definition of real and fake truth match criteria:
double_t cutoff_real = 0.5;
double_t cutoff_fake = 0.5;

//MC CUTS
double_t ptmatch = 100; // reconstructed truth matched particle pT must match truth matched kaon pT within +/- this
double_t massmatch = 10; //reconstructed truth matched particle mass must match truth matched kaon mass within +/- this

int rrand;

bool hpassThetaStarcut;
bool hpassDeltaRcut;
bool hpasspi1d0cut;
bool hpasspi2d0cut;
bool hpassxylengthcut;
bool hpasskpTcut;
bool hpi1barrel;
bool hpi2barrel;
bool fillhists; 
///////


int test; 
  //This function looks for k0 candidates..
  double drkpiMax = 0.4;  // loose
  const double mpion = 139.57;
  const double mkaon = 497.611;
  int nmatches;

  xAOD::TrackParticleContainer::const_iterator tracks_end = Tracks->end();
  for(xAOD::TrackParticleContainer::const_iterator track_iter1 = Tracks->begin() ; track_iter1!=tracks_end ; ++track_iter1) {  //pion1 loop
  if (track_iter1 == tracks_end) continue; //because we only want pion1 to loop from start to end-1
  const xAOD::TrackParticle *pion1 = *track_iter1;
test = 0;
  if ( !m_selTool->accept(pion1))  continue; //track selection
  int  charge = pion1->charge(); // Use the pion1 charge to verify other particles.
  if (charge!= 1) continue;
  TLorentzVector pion14v;
  pion14v.SetPtEtaPhiM(pion1->pt(),pion1->eta(),pion1->phi(),mpion); // Store 4vector, pion1 mass

	for(xAOD::TrackParticleContainer::const_iterator track_iter2 = track_iter1 ; track_iter2!=tracks_end ; ++track_iter2 ) { // pion2 loop
        if (track_iter2 == track_iter1) continue; //because we want pion 2 to go from pion1+1 to end
	const xAOD::TrackParticle *pion2 = *track_iter2;
test++;
	if ( !m_selTool->accept(pion2))  continue; //track selection
	if (pion2->charge() == charge) continue; // Check for opposite charge
        if  (pion14v.DeltaR( pion2->p4() ) > drkpiMax) continue; // Check similar to pion1 direction
        // OK, we have opposite sign pi/pi, at sensible spacing. Vertex them:
        TVector3 pipivertex = kvertex(pion1,pion2);
        TLorentzVector pion24v = kvertex4v(pipivertex,pion2,mpion); //pion2 4v at vtx
        pion14v = kvertex4v(pipivertex,pion1,mpion); // pion1 4v at vtx

	// Build the k0 candidate, pion1+pion2 summed
        TLorentzVector pipi4v = pion14v+pion24v;
        // Calculate the cos-theta-star (angle of pion1 in k0 rest frame)
        TVector3 boost =-pipi4v.BoostVector();
        TLorentzVector pion1boosted = pion14v;
        pion1boosted.Boost(boost);      //  pion1 momentum "boosted" into k0 frame
        TVector3 daughter(pion1boosted.Vect());
        TVector3 mother(pipi4v.Vect());
        double cosThetaStar = daughter.Dot(mother)/(daughter.Mag()*mother.Mag());
        double xylength = (pipivertex.x()*mother.Px()+ pipivertex.y()*mother.Py())/mother.Pt();

	////Fill Tree Variables
	tkaon_mpipi->push_back(pipi4v.M());
	tkaon_pipipT->push_back(pipi4v.Pt());
	tkaon_pipiDeltaR->push_back(pion14v.DeltaR( pion2->p4() ));
	tkaon_pi1d0->push_back(pion1->d0());
	tkaon_pi1phi->push_back(pion1->phi());
	tkaon_pi1eta->push_back(pion1->eta());
	tkaon_pi1pT->push_back(pion1->pt());
	tkaon_pi1sinTheta->push_back(sin( pion1->theta() ));
	tkaon_pi2d0->push_back(pion2->d0());
	tkaon_pi2phi->push_back(pion2->phi());
	tkaon_pi2eta->push_back(pion2->eta());
	tkaon_pi2pT->push_back(pion2->pt());
	tkaon_pi2sinTheta->push_back(sin( pion2->theta() ));
	tkaon_pipivertex_x->push_back(pipivertex.x());
	tkaon_pipivertex_y->push_back(pipivertex.y());
	tkaon_pipivertex_z->push_back(pipivertex.z());
	tkaon_xylength->push_back(xylength);
	tkaon_cosThetaStar->push_back(cosThetaStar);
	if(isMC){
		tkaon_pi2TruthMatchProb->push_back(pion2->auxdata< float >("truthMatchProbability"));
		tkaon_pi1TruthMatchProb->push_back(pion1->auxdata< float >("truthMatchProbability"));
		}
	
	//fill Nhit variables:
  	uint8_t nPixel=0; //, nPixelSplitHits=0;
  	uint8_t nSCT=0; //, nSCTHoles=0;
  	uint8_t nTRT=0;
  	uint8_t nPixel2=0; //, nPixelSplitHits2=0;
  	uint8_t nSCT2=0; //, nSCTHoles2=0;
  	uint8_t nTRT2=0;

  	pion1->summaryValue( nPixel, xAOD::numberOfPixelHits );
  	//pion1->summaryValue( nPixelSplitHits, xAOD::numberOfPixelSplitHits );
  	pion1->summaryValue( nSCT, xAOD::numberOfSCTHits );
  	//pion1->summaryValue( nSCTHoles, xAOD::numberOfSCTHoles );
  	pion1->summaryValue( nTRT, xAOD::numberOfTRTHits );

  	pion2->summaryValue( nPixel2, xAOD::numberOfPixelHits );
  	//pion2->summaryValue( nPixelSplitHits2, xAOD::numberOfPixelSplitHits );
  	pion2->summaryValue( nSCT2, xAOD::numberOfSCTHits );
  	//pion2->summaryValue( nSCTHoles2, xAOD::numberOfSCTHoles );
  	pion2->summaryValue( nTRT2, xAOD::numberOfTRTHits );

	tkaon_pi2Npixelhits->push_back(nPixel2);
	tkaon_pi2NSCThits->push_back(nSCT2);
	tkaon_pi2NTRThits->push_back(nTRT2);
	tkaon_pi1Npixelhits->push_back(nPixel);
	tkaon_pi1NSCThits->push_back(nSCT);
	tkaon_pi1NTRThits->push_back(nTRT);

/////////////////////////////////////////////////////
//Make cuts for Kaon mass signal and fill histograms
/////////////////////////////////////////////////////


  	hpassThetaStarcut = true;
  	hpassDeltaRcut = true;
  	hpasspi1d0cut = true;
  	hpasspi2d0cut = true;
  	hpassxylengthcut = true;
  	hpasskpTcut = true;
  	hpi1barrel = true;
  	hpi2barrel = true;
	fillhists = false; 
  	rrand = rand() % 2;

        if (abs(pion1->eta())>2.2) hpi1barrel=false;
        if (abs(pion2->eta())>2.2) hpi2barrel=false;
        if (abs(cosThetaStar) > cosThstcutmax) hpassThetaStarcut = false; //Cos(ThetaStar) is the angle btwn vertex and (pion BOOSTED in k0 frame) so backgrounds will be close to 1
        if (pion14v.DeltaR( pion2->p4() ) > DeltaRmax) hpassDeltaRcut = false;
        if (abs(pion1->d0())<d0) hpasspi1d0cut = false;
        if (abs(pion2->d0())<d0) hpasspi2d0cut = false;
        if (hpi1barrel && hpi2barrel && xylength>xylengthmin && xylength<xylengthmax) hpassxylengthcut = true;
        if (pipi4v.Pt() > kpTcutmax || pipi4v.Pt() <  kpTcutmin) hpasskpTcut = false;
        if (hpassThetaStarcut && hpassDeltaRcut && hpasspi1d0cut && hpasspi2d0cut && hpassxylengthcut && hpasskpTcut) {
		fillhists = true; 
        	if(pipi4v.M()<400 || pipi4v.M()>600) continue;
		hist ("hkmass")->Fill(pipi4v.M());

		hist ("d01vkmass")->Fill(pipi4v.M(),pion1->d0());
                hist ("d02vkmass")->Fill(pipi4v.M(),pion2->d0());
                hist ("xylengthvkmass")->Fill(pipi4v.M(),xylength);
                hist ("cosThstvkmass")->Fill(pipi4v.M(),cosThetaStar);
                hist ("DeltaRvskmass")->Fill(pipi4v.M(),pion14v.DeltaR( pion2->p4() ));
                hist ("kpTvskmass")->Fill(pipi4v.M(),pipi4v.Pt());
                hist ("pi1pTvskmass")->Fill(pipi4v.M(),pion1->pt());
                hist ("pi2pTvskmass")->Fill(pipi4v.M(),pion2->pt());
                hist ("pixhitsvskmass")->Fill(pipi4v.M(),nPixel);
                hist ("scthitsvskmass")->Fill(pipi4v.M(),nSCT);
                hist ("trthitsvskmass")->Fill(pipi4v.M(),nTRT);

		if(rrand==0){ // fill build hists

		if(pion1->auxdata< float >("truthMatchProbability")<cutoff_fake && isMC) {
			hist ("npix_MCfake")->Fill(nPixel);
                        hist ("nSCT_MCfake")->Fill(nSCT);
                        hist ("nTRT_MCfake")->Fill(nTRT);
			}
                if(pion2->auxdata< float >("truthMatchProbability")<cutoff_fake && isMC) {
                        hist ("npix_MCfake")->Fill(nPixel2);
                        hist ("nSCT_MCfake")->Fill(nSCT2);
                        hist ("nTRT_MCfake")->Fill(nTRT2);
			}
                if(pion1->auxdata< float >("truthMatchProbability")>cutoff_real && isMC) {
			hist ("npix_MCreal")->Fill(nPixel);
                        hist ("nSCT_MCreal")->Fill(nSCT);
                        hist ("nTRT_MCreal")->Fill(nTRT);
			}
                if(pion2->auxdata< float >("truthMatchProbability")>cutoff_real && isMC) {
                        hist ("npix_MCreal")->Fill(nPixel2);
                        hist ("nSCT_MCreal")->Fill(nSCT2);
                        hist ("nTRT_MCreal")->Fill(nTRT2);
			}
		if(pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess)) {
			hist ("npix_side")->Fill(nPixel);
                        hist ("npix_side")->Fill(nPixel2);
                        hist ("nSCT_side")->Fill(nSCT);
                        hist ("nSCT_side")->Fill(nSCT2);
                        hist ("nTRT_side")->Fill(nTRT);
                        hist ("nTRT_side")->Fill(nTRT2);
			}
		if(pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess)) {
			hist ("npix_sig")->Fill(nPixel);
                        hist ("npix_sig")->Fill(nPixel2);
                        hist ("nSCT_sig")->Fill(nSCT);
                        hist ("nSCT_sig")->Fill(nSCT2);
                        hist ("nTRT_sig")->Fill(nTRT);
                        hist ("nTRT_sig")->Fill(nTRT2);
			}

		}//fill build hists

		if(rrand==1){ //fill test hists
		if(pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess)) {
                        hist ("npix_side_test")->Fill(nPixel);
                        hist ("npix_side_test")->Fill(nPixel2);
                        hist ("nSCT_side_test")->Fill(nSCT);
                        hist ("nSCT_side_test")->Fill(nSCT2);
                        hist ("nTRT_side_test")->Fill(nTRT);
                        hist ("nTRT_side_test")->Fill(nTRT2);
                        }
                if(pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess)) {
                        hist ("npix_sig_test")->Fill(nPixel);
                        hist ("npix_sig_test")->Fill(nPixel2);
                        hist ("nSCT_sig_test")->Fill(nSCT);
                        hist ("nSCT_sig_test")->Fill(nSCT2);
                        hist ("nTRT_sig_test")->Fill(nTRT);
                        hist ("nTRT_sig_test")->Fill(nTRT2);
                        }
		}//fill test hists

		if (isMC) {
		        hist ("prob")->Fill(pion1->auxdata< float >("truthMatchProbability"));
		        hist ("prob")->Fill(pion2->auxdata< float >("truthMatchProbability"));
		        if(pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess)) hist ("prob_sig")->Fill(pion1->auxdata< float >("truthMatchProbability"));
		        if(pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess)) hist ("prob_sig")->Fill(pion2->auxdata< float >("truthMatchProbability"));
		        if(pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess)) hist ("prob_sideband")->Fill(pion1->auxdata< float >("truthMatchProbability"));
		        if(pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess)) hist ("prob_sideband")->Fill(pion2->auxdata< float >("truthMatchProbability"));	
		}


	} //end if cuts for hists
		
////
//Start MC Truth analysis: 
////
double_t minTruthMatchProb = 0.5; //when matching tracks to truth particles, this is the criteria for a matched track.

	if (isMC) {
	bool foundlink = false;
	ElementLink< xAOD::TruthParticleContainer > truthLink1;
        ElementLink< xAOD::TruthParticleContainer > truthLink2;
		//check that both pions have truth links: 
		if (pion1->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") ){
		truthLink1 = pion1->auxdata< ElementLink< xAOD::TruthParticleContainer>  >("truthParticleLink");
		if (pion2->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") ){
		truthLink2 = pion2->auxdata< ElementLink< xAOD::TruthParticleContainer>  >("truthParticleLink");
		  if(truthLink1.isValid() && truthLink2.isValid()){
		    foundlink = true;
		  }
		}}

	//Now we know we have truth links and matched tracks, fill variables and hists: 
	
	ttruth_isLinked->push_back(foundlink);	

	if (foundlink) {
	  ttruth_pdgId1->push_back((*truthLink1)->pdgId());
          ttruth_pdgId2->push_back((*truthLink2)->pdgId());
	  ttruth_barcode1->push_back((*truthLink1)->barcode());
	  ttruth_barcode2->push_back((*truthLink2)->barcode());
	} else { //if no link, fill variable with meaningless number
	  ttruth_pdgId1->push_back(0);
	  ttruth_pdgId2->push_back(0);
	  ttruth_barcode1->push_back(0);
	  ttruth_barcode2->push_back(0);
	}

	bool isSoftTruthMatched = false;

	//check that both pions have the min truth match prob requirement: 
	if (foundlink && pion1->auxdata<float>("truthMatchProbability") > minTruthMatchProb && pion2->auxdata<float>("truthMatchProbability") > minTruthMatchProb){
	
		//check that both pions are truth matched pions:
		if ( abs((*truthLink1)->pdgId()) == 211 && abs((*truthLink2)->pdgId()) == 211 ) {

//	----begin Kaon soft truth match search
		//loop over tracks to find truth matched kaons: 
		for( xAOD::TrackParticleContainer::const_iterator track_iter3 = Tracks->begin() ; track_iter3!=tracks_end ; ++track_iter3  ) { //kaon loop
		const xAOD::TrackParticle *kaon = *track_iter3;

		//create truth link for kaon
		ElementLink< xAOD::TruthParticleContainer > truthLink3;
		if (kaon->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") ){
		truthLink3 = kaon->auxdata< ElementLink< xAOD::TruthParticleContainer>  >("truthParticleLink");
		} else continue;

		if (!truthLink3.isValid()) continue;

		if ( abs((*truthLink3)->pdgId()) != 310) continue; //check the track is a kaon

		TLorentzVector kaon4v;
  		kaon4v.SetPtEtaPhiM(kaon->pt(),kaon->eta(),kaon->phi(),mkaon);
 
		//hists of truth matched kaons: 
		hist("truthKaonpT")->Fill(kaon->pt());
		hist("truthKaonDeltaR")->Fill(pipi4v.DeltaR( kaon->p4() ));
		
		if ( ((kaon->pt() - ptmatch )  > pipi4v.Pt()) || (pipi4v.Pt() < (kaon->pt() + ptmatch )) ) continue; 
		if ( ((kaon->m() - massmatch ) >pipi4v.M()) || (pipi4v.M() < (kaon->m() + massmatch )) ) continue;
		
		isSoftTruthMatched = true;
				
		}//end kaon loop
//      ----end Kaon soft truth match search 

		if (isSoftTruthMatched) { 
if (test==1) nmatches=0;
nmatches++;
std::cout << "Found " << nmatches << "soft truth matches" << std::endl;
		// both pions are from kshort, fill variables (variables filled regardless of cut values results
			ttruth_isKshortpion1->push_back(true);
			ttruth_isKshortpion2->push_back(true);
		//fill truth kaon event hists (tke hists): 
			if (fillhists) { //fillhists is boolean that it passed all cuts for the histograms above
			hist ("hkmass_tke")->Fill(pipi4v.M());
			hist ("npix_tke_all")->Fill(nPixel);
			hist ("nSCT_tke_all")->Fill(nSCT);
			hist ("nTRT_tke_all")->Fill(nTRT);

			hist ("d01vkmass_tke")->Fill(pipi4v.M(),pion1->d0());
                	hist ("d02vkmass_tke")->Fill(pipi4v.M(),pion2->d0());
                	hist ("xylengthvkmass_tke")->Fill(pipi4v.M(),xylength);
                	hist ("cosThstvkmass_tke")->Fill(pipi4v.M(),cosThetaStar);
                	hist ("DeltaRvskmass_tke")->Fill(pipi4v.M(),pion14v.DeltaR( pion2->p4() ));
                	hist ("kpTvskmass_tke")->Fill(pipi4v.M(),pipi4v.Pt());
                	hist ("pi1pTvskmass_tke")->Fill(pipi4v.M(),pion1->pt());
                	hist ("pi2pTvskmass_tke")->Fill(pipi4v.M(),pion2->pt());
                	hist ("pixhitsvskmass_tke")->Fill(pipi4v.M(),nPixel);
                	hist ("scthitsvskmass_tke")->Fill(pipi4v.M(),nSCT);
                	hist ("trthitsvskmass_tke")->Fill(pipi4v.M(),nTRT);
			}

			if(fillhists && (pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess))) {
				hist ("npix_sig_tke")->Fill(nPixel);
				hist ("nSCT_sig_tke")->Fill(nSCT);
				hist ("nTRT_sig_tke")->Fill(nTRT);
			}
	
			if(fillhists && (pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess))) {
				hist ("npix_side_tke")->Fill(nPixel);
				hist ("nSCT_side_tke")->Fill(nSCT);
				hist ("nTRT_side_tke")->Fill(nTRT);
			}

		} //end if isSoftTruthMatched
		} //end if for truth matched pions

		if (!isSoftTruthMatched) {
		// fill NOTtke hists
			ttruth_isKshortpion1->push_back(false);
                        ttruth_isKshortpion2->push_back(false);
			if (fillhists) {
			hist ("hkmass_NOTtke")->Fill(pipi4v.M());
			hist ("npix_NOTtke_all")->Fill(nPixel);
                        hist ("nSCT_NOTtke_all")->Fill(nSCT);
                        hist ("nTRT_NOTtke_all")->Fill(nTRT);

			hist ("d01vkmass_NOTtke")->Fill(pipi4v.M(),pion1->d0());
                        hist ("d02vkmass_NOTtke")->Fill(pipi4v.M(),pion2->d0());
                        hist ("xylengthvkmass_NOTtke")->Fill(pipi4v.M(),xylength);
                        hist ("cosThstvkmass_NOTtke")->Fill(pipi4v.M(),cosThetaStar);
                        hist ("DeltaRvskmass_NOTtke")->Fill(pipi4v.M(),pion14v.DeltaR( pion2->p4() ));
                        hist ("kpTvskmass_NOTtke")->Fill(pipi4v.M(),pipi4v.Pt());
                        hist ("pi1pTvskmass_NOTtke")->Fill(pipi4v.M(),pion1->pt());
                        hist ("pi2pTvskmass_NOTtke")->Fill(pipi4v.M(),pion2->pt());
                        hist ("pixhitsvskmass_NOTtke")->Fill(pipi4v.M(),nPixel);
                        hist ("scthitsvskmass_NOTtke")->Fill(pipi4v.M(),nSCT);
                        hist ("trthitsvskmass_NOTtke")->Fill(pipi4v.M(),nTRT);

			}
			if(fillhists && pipi4v.M()>(mean_guess-2*sigma_guess) && pipi4v.M()<(mean_guess+2*sigma_guess)) {
                                hist ("npix_sig_NOTtke")->Fill(nPixel);
                                hist ("nSCT_sig_NOTtke")->Fill(nSCT);
                                hist ("nTRT_sig_NOTtke")->Fill(nTRT);
                        }

                        if(fillhists && (pipi4v.M()<(mean_guess-3*sigma_guess) || pipi4v.M()>(mean_guess+3*sigma_guess))) {
                                hist ("npix_side_NOTtke")->Fill(nPixel);
                                hist ("nSCT_side_NOTtke")->Fill(nSCT);
                                hist ("nTRT_side_NOTtke")->Fill(nTRT);
                        }
		} //end if !isSoftTruthMatched

	} //end if(foundlink...

	} //end isMC truth analysis 


	}//end pion2 loop
  }//end pion1 loop

  // Fill the event into the tree:
  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode KaonxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // output some data on how many tracks passed each type of cut
  // can be useful for debugging

  ANA_CHECK (m_selTool->finalize());
  
  tree ("analysis")->Print();

  if (ttrack_charge) delete ttrack_charge;
  if (ttrack_pT) delete ttrack_pT;
  if (ttrack_eta) delete ttrack_eta;
  if (ttrack_phi) delete ttrack_phi;
  if (ttrack_E) delete ttrack_E;
  if (ttrack_d0) delete ttrack_d0;
  if (ttrack_sinTheta) delete ttrack_sinTheta;
  if (ttrack_Npixelhits) delete ttrack_Npixelhits;
  if (ttrack_NSCThits) delete ttrack_NSCThits;
  if (ttrack_NTRThits) delete ttrack_NTRThits;
  if (tpdgId) delete tpdgId;
  if (ttrack_TruthMatchProb) delete ttrack_TruthMatchProb;
  if (tkaon_mpipi) delete tkaon_mpipi;
  if (tkaon_pipipT) delete tkaon_pipipT;
  if (tkaon_pipiDeltaR) delete tkaon_pipiDeltaR;
  if (tkaon_pi1d0) delete tkaon_pi1d0;
  if (tkaon_pi1phi) delete tkaon_pi1phi;
  if (tkaon_pi1eta) delete tkaon_pi1eta;
  if (tkaon_pi1pT) delete tkaon_pi1pT;
  if (tkaon_pi1sinTheta) delete tkaon_pi1sinTheta;
  if (tkaon_pi2d0) delete tkaon_pi2d0;
  if (tkaon_pi2phi) delete tkaon_pi2phi;
  if (tkaon_pi2eta) delete tkaon_pi2eta;
  if (tkaon_pi2pT) delete tkaon_pi2pT;
  if (tkaon_pi2sinTheta) delete tkaon_pi2sinTheta;
  if (tkaon_pipivertex_x) delete tkaon_pipivertex_x;
  if (tkaon_pipivertex_y) delete tkaon_pipivertex_y;
  if (tkaon_pipivertex_z) delete tkaon_pipivertex_z;
  if (tkaon_xylength) delete tkaon_xylength;
  if (tkaon_cosThetaStar) delete tkaon_cosThetaStar;
  if (tkaon_pi2Npixelhits) delete tkaon_pi2Npixelhits;
  if (tkaon_pi2NSCThits) delete tkaon_pi2NSCThits;
  if (tkaon_pi2NTRThits) delete tkaon_pi2NTRThits;
  if (tkaon_pi2TruthMatchProb) delete tkaon_pi2TruthMatchProb;
  if (tkaon_pi1Npixelhits) delete tkaon_pi1Npixelhits;
  if (tkaon_pi1NSCThits) delete tkaon_pi1NSCThits;
  if (tkaon_pi1NTRThits) delete tkaon_pi1NTRThits;
  if (tkaon_pi1TruthMatchProb) delete tkaon_pi1TruthMatchProb;

  if (ttruth_isLinked) delete ttruth_isLinked;
  if (ttruth_isKshortpion1) delete ttruth_isKshortpion1;
  if (ttruth_isKshortpion2) delete ttruth_isKshortpion2;
  if (ttruth_pdgId1) delete ttruth_pdgId1;
  if (ttruth_pdgId2) delete ttruth_pdgId2;
  if (ttruth_barcode1) delete ttruth_barcode1;
  if (ttruth_barcode2) delete ttruth_barcode2;


  return StatusCode::SUCCESS;
}



 KaonxAODAnalysis :: ~KaonxAODAnalysis () {

}

