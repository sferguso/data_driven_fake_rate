#ifndef KaonAnalysis_KaonxAODAnalysis_H
#define KaonAnalysis_KaonxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TTree.h>
#include <vector>
#include <TH1.h>
#include <TH2.h>

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

//TrackSelectionTool
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>

class KaonxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  KaonxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~KaonxAODAnalysis();
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_selTool; //!

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  Int_t 	tRunNumber = 0; //!
  Int_t 	tEventNumber = 0; //!
  Float_t 	tAvgMu = 0; //!
  Int_t 	tNtracks = 0; //!
  Int_t 	m_numevents =0; //!
  std::vector<int> 	*ttrack_charge = nullptr; //!
  std::vector<float> 	*ttrack_pT = nullptr; //!
  std::vector<float>	*ttrack_eta = nullptr; //!
  std::vector<float>	*ttrack_phi = nullptr; //!
  std::vector<float>	*ttrack_E = nullptr; //!
  std::vector<float>	*ttrack_d0 = nullptr; //!
  std::vector<float>	*ttrack_sinTheta = nullptr; //!
  std::vector<int>	*ttrack_Npixelhits = nullptr; //!
  std::vector<int>	*ttrack_NSCThits = nullptr; //!
  std::vector<int>	*ttrack_NTRThits = nullptr; //!
  std::vector<int>	*tpdgId = nullptr; //!
  std::vector<float>	*ttrack_TruthMatchProb = nullptr; //!
  std::vector<float>	*tkaon_mpipi = nullptr; //!
  std::vector<float>	*tkaon_pipipT = nullptr; //!
  std::vector<float>	*tkaon_pipiDeltaR = nullptr; //!
  std::vector<float>	*tkaon_pi1d0 = nullptr; //!
  std::vector<float>	*tkaon_pi1phi = nullptr; //!
  std::vector<float>	*tkaon_pi1eta = nullptr; //!
  std::vector<float>	*tkaon_pi1pT = nullptr; //! 
  std::vector<float>	*tkaon_pi1sinTheta = nullptr; //!
  std::vector<float>	*tkaon_pi2d0 = nullptr; //!
  std::vector<float>    *tkaon_pi2phi = nullptr; //!
  std::vector<float>    *tkaon_pi2eta = nullptr; //!
  std::vector<float>    *tkaon_pi2pT = nullptr; //!
  std::vector<float>    *tkaon_pi2sinTheta = nullptr; //!
  std::vector<float>    *tkaon_pipivertex_x = nullptr; //!
  std::vector<float>    *tkaon_pipivertex_y = nullptr; //!
  std::vector<float>    *tkaon_pipivertex_z = nullptr; //!
  std::vector<float>    *tkaon_xylength = nullptr; //!
  std::vector<float>    *tkaon_cosThetaStar = nullptr; //!
  std::vector<int>      *tkaon_pi2Npixelhits = nullptr; //!
  std::vector<int>      *tkaon_pi2NSCThits = nullptr; //!
  std::vector<int>	*tkaon_pi2NTRThits = nullptr; //!
  std::vector<float>	*tkaon_pi2TruthMatchProb = nullptr; //!
  std::vector<int>	*tkaon_pi1Npixelhits = nullptr; //!
  std::vector<int>      *tkaon_pi1NSCThits = nullptr; //!
  std::vector<int>      *tkaon_pi1NTRThits = nullptr; //! 
  std::vector<float>	*tkaon_pi1TruthMatchProb = nullptr; //!

  std::vector<bool>	*ttruth_isLinked = nullptr; //!
  std::vector<bool> 	*ttruth_isKshortpion1 = nullptr; //!
  std::vector<bool>     *ttruth_isKshortpion2 = nullptr; //!
  std::vector<int>	*ttruth_pdgId1 = nullptr; //!
  std::vector<int>      *ttruth_pdgId2 = nullptr; //!
  std::vector<int>      *ttruth_barcode1 = nullptr; //!
  std::vector<int>      *ttruth_barcode2 = nullptr; //!


  TH1D *hkmass; //!

  TH1D *npix_MCreal; //!
  TH1D *npix_MCfake; //!
  TH1D *npix_side; //!
  TH1D *npix_sig; //!
  TH1D *npix_sig_test; //!
  TH1D *npix_side_test; //!

  TH1D *nSCT_MCreal; //!
  TH1D *nSCT_MCfake; //!
  TH1D *nSCT_side; //!
  TH1D *nSCT_sig; //!
  TH1D *nSCT_sig_test; //!
  TH1D *nSCT_side_test; //!

  TH1D *nTRT_MCreal; //!
  TH1D *nTRT_MCfake; //!
  TH1D *nTRT_side; //!
  TH1D *nTRT_sig; //!
  TH1D *nTRT_sig_test; //!
  TH1D *nTRT_side_test; //!

  TH1D *prob; //!
  TH1D *prob_sig; //!
  TH1D *prob_sideband; //!

  TH2D *d01vkmass; //!
  TH2D *d02vkmass; //!
  TH2D *xylengthvkmass; //!
  TH2D *cosThstvkmass; //!
  TH2D *DeltaRvskmass; //!
  TH2D *kpTvskmass; //!
  TH2D *pi1pTvskmass; //!
  TH2D *pi2pTvskmass; //!
  TH2D *pixhitsvskmass; //!
  TH2D *pix1hitsvskmass; //!
  TH2D *pix2hitsvskmass; //!


};

#endif
