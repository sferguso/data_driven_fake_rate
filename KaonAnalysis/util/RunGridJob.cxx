#include "xAODRootAccess/Init.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleHandler.h"
#include <SampleHandler/ToolsDiscovery.h>
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <TH1.h>

void RunGridJob (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

//SH::scanRucio (sh, "data17_13TeV:data17_13TeV.00340072.physics_Main.recon.DAOD_IDTIDE.r10426");
//SH::scanRucio (sh, "data17_13TeV:data17_13TeV.00340030.physics_Main.recon.DAOD_IDTIDE.r10426");
//SH::scanRucio (sh, "data17_13TeV:data17_13TeV.00340453.physics_Main.recon.DAOD_IDTIDE.r10426");
//SH::scanRucio (sh, "data17_13TeV:data17_13TeV.00340368.physics_Main.recon.DAOD_IDTIDE.r10426");

//MC16a: 
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.recon.DAOD_IDTIDE.e3668_s2997_r9668/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.recon.DAOD_IDTIDE.e3668_s2997_r9668/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.recon.DAOD_IDTIDE.e3668_s2997_r9668/");

//MC16c: 
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.recon.DAOD_IDTIDE.e3668_s3126_r10038/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.recon.DAOD_IDTIDE.e3668_s3126_r10038/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.recon.DAOD_IDTIDE.e3668_s3126_r10038/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.recon.DAOD_IDTIDE.e3668_s3126_r10038/");
//SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.recon.DAOD_IDTIDE.e3668_s3126_r10038/");

SH::scanRucio (sh, "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.recon.DAOD_IDTIDE.e3668_s3126_r10707");

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  EL::AnaAlgorithmConfig config;
  config.setType ("KaonxAODAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  config.setName ("AnalysisAlg");

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (config);
  job.outputAdd (EL::OutputStream ("ANALYSIS"));

  // make the driver we want to use:
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.sferguso.MC16old_fakestudy_18Feb2019.%in:name[2]%.%in:name[6]%");

  // process the job using the driver
  driver.submit (job, submitDir);
}

